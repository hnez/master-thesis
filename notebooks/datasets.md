---
title: Datasets
lang: de-DE
...


Name          Synthetisch Video Segmentation Depth Stereo
----          ----------- ----- ------------ ----- ------
Synthia       ✓           5fps  ✓            ✓     sehr
GTA-IV        ✓           ✕     ✓            ✕     ✕
Virtual KITTI ✓           5fps  ✓            ✓     ✕
Cityscapes    ✕           17fps ✓            ✕     ✓
KITTI         ✕           10fps kaum         lidar ✓

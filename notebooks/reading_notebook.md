---
title: Reading Notebook
lang: de-DE
...

Tools
=====

```python
author= "Someone"
title= "A tool Demo"

# Filename:
author + '_' + title.replace(' ', '_') + '.pdf'

# Single Letter title digest:
(author.lower() + '_' + ''.join(w.strip()[0] for w in title.split())).lower()
```

16.10.2019
==========

Paper [Unsupervised Learning of Depth and Ego-Motion from Video][zhou_ulodaemfv]
--------------------------------------------------------------------------------

- Führt ein Tiefen-CNN und ein Pose-CNN ein
- Aus Tiefe und Pose werden Bildpunkte in einen 3D Raum gemapped
- Das wird auch für ein Bild in einer anderen Pose gemacht.
- Das Bild wird so verzerrt, dass es erscheint wie in der anderen Pose aufgenommen.
- Das Bild der anderen Pose dient als Vorlage zum Training der Tiefen und Pose-Netze.
- Z.B. überlappende und bewegte Bereiche lassen sich nicht beschreiben.
  Diese werden beim Training ausgeblendet

Paper [Digging Into Self-Supervised Monocular Depth Estimation][godart_dissmde]
-------------------------------------------------------------------------------

- Verbesserungen aufbauend auf Zhou Paper
- Unbewegte Pixel ausmaskieren
- Minimalen Reprojection Loss forcieren (TODO: watt?)

Paper [Towards Scene Understanding: Unsupervised Monocular Depth Estimation with Semantic-aware Representation][chen_tsuumdewsar]
---------------------------------------------------------------------------------------------------------------------------------

- Stellt Zusammenhang zwischen Depth Estimation und Semantischer Segmentierung her
- Vermeidung der Notwendigkeit für Bilder Annotiert mit Klassen _und_ Tiefe
  bzw. Annotierte Videosequenzen
- Nutzt Stereo-Bilder
- Gemeinsames Encoder-Netz und zwei Dekoder für Segmentierung/Tiefenschätzung

Paper [Unsupervised Domain Adaptation to Improve Image Segmentation Quality Both in the Source and Target Domain][bolte_udatiisqbic]
------------------------------------------------------------------------------------------------------------------------------------

- Ein auf einem Datensatz trainiertes Netz funktioniert nicht so gut auf einem anderen Datensatz
  für den z.B. nicht trainiert werden kann, weil Labels fehlen
- Ziel: Netz erzeugen, dass datensatzunabhängig funktioniert
- Es sollen tatsächliche Features erkannt werden und nicht Artefakte des Datensatzes
- Features sind ein latent space zwischen Feature Extractor und tatsächlicher Segmentierung
- Ansatz: Domain Discriminator darauf trainieren aus den extrahierten Features die Quell-Domain zu erkennen
- Vor dem Domain Discriminator eine Gradient Reversal Layer einsetzen, die dafür sorgt,
  dass der Feature Extractor in eine Richtung trainiert wird, die es dem Domain Discriminator
  _schwieriger_ macht zu erkennen wo das Quellbild her kommt. (Cool, oder?)
- Versuch:
    - Der Cityscapes Datensatz enthält eine große Menge gelabelter Daten
    - Der Kitti Datensatz enthält deutlich mehr Bilder, jedoch nur wenige gelabelte
    - Cityscapes konnte genutzt werden, um die Kitti Ergebnisse deutlich zu verbessern

21.10.2019
==========

Präsentation [Tiefenschätzung mittels Bildsequenz einer Monokamera - Ein Überblick][mikolajczyk_tmbemeu]
--------------------------------------------------------------------------------------------------------

- Überblick über die Datensätze
- Nette Grafiken zu den Netzwerken

22.10.2019
==========

Paper [Learning from Synthetic Data: Addressing Domain Shift for Semantic Segmentation][sankaranarayanan_lfsdadsfss]
--------------------------------------------------------------------------------------------------------------------

- Nutzt SYNTHIA, CITYSCAPES und GTA-5
- GAN basiert

Paper [CyCADA: Cycle-Consistent Adversial Domain Adaptation][hoffman_ccada]
---------------------------------------------------------------------------

- Cycle GAN Eingangsbilder zwischen Domains zu konvertieren
- Irgendwas mit Semantischem Loss

22.10.2019
==========

Paper [Maximum Classifier Discrepancy for Unsupervised Domain Adaptation][saito_mcdfuda]
----------------------------------------------------------------------------------------

- Viel Theorie zu Features in verschiedenen Domains
- Tatsächliches Vorgehen verstehe ich noch nicht (TODO)

Paper [Domain Adaptation for Semantic Segmentation via Class-Balanced Self-Training][zou_dafss]
-----------------------------------------------------------------------------------------------

- Curriculum Learning - Das Netz erzeugt Segmentierung, entscheidet wie viel Vertrauen es darin hat
  und behandelt die Guten als Labels fürs Training
- Spatial priors - Die Klassen sind recht berechenbar im Bild verteilt. Himmel ist oben, Straßen unten links, usw..
  Das kann man nutzen
- Kein Diagramm des Netzwerkes 😒

Paper Self-Supervised Monocular Depth Estimation: Towards Solving the Moving Object Problem by Semantic Guidance (Kein .pdf)
----------------------------------------------------------------------------------------------------------------------------

-

Paper [Depth Prediction Without the Sensors: Leveraging Structure for Unsupervised Learning from Monocular Videos][casser_dpwtslsfulfmv]
----------------------------------------------------------------------------------------------------------------------------------------

- Mit Object motion
- Minimum Reconstruction loss hilft Occlusionseffekte zu mitigieren, indem
  min(diff(warp(nächstes\_frame), aktuelles\_frame), diff(warp(corheriges\_frame), aktuelles\_frame))
  als Loss verwendet wird. (Annahme: Occlusion findet nur in einem der beiden Frames statt)
- Jedes Objekt wird separat Bewegungsgeschätzt
- Erst: Ausmaskieren der bewegten Objekte (wie bei uns)
- Interessantes Problem: Autos direkt vor Kamerawagen werden am Besten als unendlich weit entfernt
  modelliert.
- Ansatz: Höhe von Autos begrenzen (Wie hab ich noch nicht verstanden)
- Domain Adaption durch online Updates

Paper [GeoNet: Unsupervised Learning of Dense Depth, Optical Flow and Camera Pose][yin_guloddofacp]
---------------------------------------------------------------------------------------------------

Paper [Image Quality Assessment: From Error Visibility to Structural Similarity][wang_iqafevtss]
------------------------------------------------------------------------------------------------

- Structural similarity (SSIM)
- Soll Zwei Bilder inhaltlich auf Ähnlichkeit untersuchen
- Andere Paper nutzen das, führen die Statistik aber nur sehr lokal durch (3x3 z.B.)
- Mal gedacht gewesen um Qualitätsverlust durch Bildkompression quantitativ beschreiben zu können.
- Differenzierbar

Paper [Every Pixel Counts: Unsupervised Geometry Learning with Holistic 3D Motion Understanding][yang_epcuglwh3mu]
------------------------------------------------------------------------------------------------------------------

- Per-Pixel Dense 3D flow
- Beziehen Stereo Bilder mit ein
- Optical flow auf den Bildern

25.10.2019
==========

Paper [Unsupervised Domain Adaptation with Similarity Learning][pinheiro_udawsl]
--------------------------------------------------------------------------------

- Netzwerk gibt einen Vektor aus, der mit anderen Vektoren verglichen werden kann.
  Für jede Klasse gibt es Prototypen anhand derer einsortiert werden kann.
- Nutzt GRL

Paper [Unsupervised Domain Adaptation by Backpropagation][ganin_udabb]
----------------------------------------------------------------------

- Führt GRL ein

28.10.2019
==========

Paper [Spatial Transformer Networks][jaderberg_stn]
---------------------------------------------------

- Führt bilineares Sampling ein
- Ist übrigens in PyTorch als `grid_sample` implementiert

Paper [Adversarial Domain Adaptation with a Domain Similarity Discriminator for Semantic Segmentation of Urban Areas][yan_adawadsdfssoua]
-----------------------------------------------------------------------------------------------------------------------------------------

- Semantische Segmentierung von Luftbildern
- Hab die Idee nicht verstanden

Paper [FlowNet: Learning Optical Flow with Convolutional Networks][dosovitskiy_flofwcn]
---------------------------------------------------------------------------------------

- FlowNet
- Eigene Correlation Layer (Bringt nichts)
- Ziemlich großes Netz, will sehr viele Objekte tracken können

Paper [Learning to Segment Moving Objects in Videos][fragkiadaki_ltsmoiv]
-------------------------------------------------------------------------

- Komplexes System um Objekte zu tracken (glaube ich)

Paper [Domain-Adversarial Training of Neural Networks][ganin_dtonn]
-------------------------------------------------------------------

- Unnormal lang
- Geht tiefer als das andere Ganin Paper (beim Überfliegen aber keine neuen Erkenntnisse)

Paper [A Large Dataset to Train Convolutional Networks for Disparity, Optical Flow, and Scene Flow Estimation][mayer_aldttcnfdofasfe]
-------------------------------------------------------------------------------------------------------------------------------------

- Aufbauend auf FlowNet
- Dataset ist für uns nicht relevant

Paper [Pixel-Level Matching for Video Object Segmentation using Convolutional Neural Networks][yoon_pmfvosucnn]
---------------------------------------------------------------------------------------------------------------

- Objektumrisse durch Videos tracken

Paper [Cross-Domain Self-supervised Multi-task Feature Learning using Synthetic Imagery][ren_csmflusi]
------------------------------------------------------------------------------------------------------

- Multi-Task Learning auf Synthetischen Daten (Kanten zwischen semantischen Segmentierungen, Flächennormale, Tiefe)
- Domain Adaption zu echten Bildern komplett ohne Labels

Paper [Playing for Data: Ground Truth from Computer Games][richter_pfdgtfcg]
----------------------------------------------------------------------------

- Heftiges Dings gefrickelt um Semantische Segmentierung aus GTA-V zu extrahieren
- 25k Bilder
- "Annotating the presented dataset with the approach used for CamVid or Cityscapes would have taken at least 12 person-years."
- Nur jedes 40. Frame ist verfügbar?

04.11.2019
==========

Paper [How Do Neural Networks See Depth in Single Images?][vandijk_hdnnsdisi]
-----------------------------------------------------------------------------

- These: Netze nutzen die vertikale Pixel-Position der Objekte statt ihrer erwarteten Größe
  das ist ein Problem, wenn sich die Kameraposition ändert
- Anmerkung: In synthetischen Datasets könnte man die Kamera zur Augmentation frei bewegen

Paper [Visualization of Convolutional Neural Networks for Monocular Depth Estimation][hu_vocnnfmde]
---------------------------------------------------------------------------------------------------

- Ist (wie das can Dijk Paper) das erste, das untersucht wie Depth Estimation funktioniert (tihi)
- Zeigt die, für die Schätzung relevanten Pixel
- Ein NN schaut sich das Eingangsbild an und erzeugt eine Maske der Pixel, die es für wichtig hält.
  Das Bild wird mit dieser Maske multipliziert.
- Losses: Unterschied der Prädiktion zu der ohne Maske, Besetztheit der Maske

Paper [Digging Into Self-Supervised Monocular Depth Estimation (ICCV)][godart_dissmdeiccv]
------------------------------------------------------------------------------------------

- Per-Pixel minimum reprojection zur Behandlung von Occlusion
- Full-resolution multi-scale - Höher aufgelöste Tiefenkarten sollen das
  Warping nur verbessern können, deshalb wird es mit allen Auflösungen durchgeführt
- Automasking entfernt statische Pixel

Paper [SynDeMo: Synergistic Deep Feature Alignment for Joint Learning of Depth and Ego-Motion][bozorgtabar_sdfafjlodem]
-----------------------------------------------------------------------------------------------------------------------

- Domain Adaption von synthetischen zu echten Bildern.
- Snythehische Bilder stellen die Baseline mit echten physikalischen Einheiten
  für die Depth Ground Truth
- Ein zusätzliches Netz (m) übersetzt Extrahierte Features für die echten Bilder,
  um sie ununterscheidbar von den Synthehischen zu machen.
  Nutzt Discriminator (p) für GAN loss.

Paper [DADA: Depth-Aware Domain Adaptation in Semantic Segmentation][vu_dadaiss]
--------------------------------------------------------------------------------

- Depth als Mittel der Domain Adaption
- Von Synthia zu Cityscapes oder Mapillary Vistas
- Depth geht direkt in semantische Segmentierung mit ein und teilt sich nicht
  nur den Encoder
- Depth Ground-Truth geht mit in das Source Training ein (Deshalb kein GTA-V?)
- Adversarial Training _auf den Ausgängen_
  (We hereby question the plausibility of adapting such a methodology for an auxiliary space)
- Vergleicht nur mit anderen Domain Adaptions, nicht damit wie gut es im Vergleich,
  mit auf echten semantischen labels, trainierten Netzen ist

Paper [SPIGAN: Privileged Adversarial Learning from Simulation][lee_spalfs]
---------------------------------------------------------------------------

- Hat DADA beeinflusst
- Privileged Information (Zusatzinformation, die sich aus dem Simulator ziehen lässt und in der target
  Domain nicht verfügbar ist)
- Image-to-image GAN um von echt zu synthehisch aussehenden Bilder zu transformieren und andersherum
  (das ist doch öde 🤷)

Paper [Depth from Videos in the Wild: Unsupervised Monocular Depth Learning from Unknown Cameras][gordon_dfvidw]
----------------------------------------------------------------------------------------------------------------

- Nehmen Kamera Intrinsics (Brennweite, dies/das) nicht als gegeben an, sondern lernen sie zu erkennen
- Occlusion wird geometrisch behandelt 😭
- Erstellen eine einzige Maske der Objekte, die sich bewegen könnten 😭
  Maske hat niedrige Auflösung. Translation Maske hat hohe Auflösung.
- Modellieren Bewegung im 3D-Raum 😭
- Ein großes Motion Prediction Network ereugt Kamera Intrinsics, Drehung, Bewegung des Hintergrunds und
  per-Pixel Bewegungen
- Führen alles für Bild1 -> Bild2 und Bild2 -> Bild1 durch und fordern, dass die Transformationen
  genau umgekehrt sind.
- Allerhand Smoothness losses
- In other words, only if a transformed pixel on the source frame lands in front of the depth map in
  the target frame, do we include that pixel in the loss.
- handle occlusions by replacing all averaging operations by a weighted averaging,
  where the weight of a pixel is a decreasing function of the depth error in that pixel
- Hatten Probleme mit Batch Normalization
- Object detection network für bewegliche Objekte ist "einfach da"

07.11.2019
==========

Paper [U-Net: Convolutional Networks for Biomedical Image Segmentation][ronneberger_ucnfbis]
--------------------------------------------------------------------------------------------

- Fully convolution network zur medizinischen Segmentierung
- Encoder und Decoder mit Durchgriffen

Paper [Deep Residual Learning for Image Recognition][he_drlfir]
---------------------------------------------------------------

- Das ResNet Paper

19.11.2019
==========

Paper [Virtual Worlds as Proxy for Multi-Object Tracking Analysis][gaidon_vwapfmota]
------------------------------------------------------------------------------------

- Virtual KITTI
- Real-to-virtual cloning
- accurate ground truth for object detection, tracking,
  scene and instance segmentation, depth, and optical flow
- Einige KITTI Szenen direkt nachgebaut und zusätzliche andere Szenen
- Tiefe ist roher Z-Buffer Wert

Paper [The SYNTHIA Dataset: A Large Collection of Synthetic Images for Semantic Segmentation of Urban Scenes][ros_tsdalcosifssous]
----------------------------------------------------------------------------------------------------------------------------------

- Verschiedene Datasets. Interessant sind insbesondere die Video sets
- Selbe Szenerie und Route wurde bei unterschiedlichen Witterungsbedingungen durchgespielt
- Depth ground truth
- Tiefe ist Entfernung zum Kameramittelpunkt
- Viele Kameras
- 5fps

Paper [The Cityscapes Dataset for Semantic Urban Scene Understanding][cordts_tcdfsusu]
--------------------------------------------------------------------------------------

- 3475 x Fine annotation
- 20000 x Coarse annotation
- Ein Bild aus einer 30-frame Sequenz annotiert

Paper [Vision meets Robotics: The KITTI Dataset][geiger_vmrtkd]
---------------------------------------------------------------

- 10Hz Video

[bolte_udatiisqbic]: ../literature/papers/Bolte_Unsupervised_Domain_Adaptation_to_Improve_Image_Segmentation_Quality_Both_in_CVPRW_2019_paper.pdf
[chen_tsuumdewsar]: ../literature/papers/Chen_Towards_Scene_Understanding_Unsupervised_Monocular_Depth_Estimation_With_Semantic-Aware_Representation_CVPR_2019_paper.pdf
[godart_dissmde]: ../literature/papers/Godard_Digging_Into_Self-Supervised_Monocular_Depth_Estimation.pdf
[zhou_ulodaemfv]: ../literature/papers/Zhou_Unsupervised_Learning_of_Depth_and_Ego-Motion_from_Video.pdf
[mikolajczyk_tmbemeu]: ../literature/presentations/Mikolajczyk_Tiefenschätzung_mittels_Bildsequenz_einer_Monokamera_-_Ein_Überblick.pdf
[sankaranarayanan_lfsdadsfss]: ../literature/papers/Sankaranarayanan_Learning_from_Synthetic_Data_Addressing_Domain_Shift_for_Semantic_Segmentation.pdf
[hoffman_ccada]: ../literature/papers/Hoffman_CYCADA_CYCLE-CONSISTENT_ADVERSARIAL_DOMAIN_ADAPTATION.pdf
[saito_mcdfuda]: ../literature/papers/Saito_Maximum_Classifier_Discrepancy_for_Unsupervised_Domain_Adaptation.pdf
[zou_dafss]: ../literature/papers/Zou_Domain_Adaptation_for_Semantic_Segmentation.pdf
[casser_dpwtslsfulfmv]: ../literature/papers/Casser_Depth_Prediction_Without_the_Sensors_Leveraging_Structure_for_Unsupervised_Learning_from_Monocular_Videos.pdf
[yin_guloddofacp]: ../literature/papers/Yin_GeoNet_Unsupervised_Learning_of_Dense_Depth,_Optical_Flow_and_Camera_Pose.pdf
[wang_iqafevtss]: ../literature/papers/Wang_Image_Quality_Assessment_From_Error_Visibility_to_Structural_Similarity.pdf
[yang_epcuglwh3mu]: ../literature/papers/Yang_Every_Pixel_Counts_Unsupervised_Geometry_Learning_with_Holistic_3D_Motion_Understanding.pdf
[pinheiro_udawsl]: ../literature/papers/Pinheiro_Unsupervised_Domain_Adaptation_CVPR_2018_paper.pdf
[ganin_udabb]: ../literature/papers/Ganin_Unsupervised_Domain_Adaptation_by_Backpropagation.pdf
[jaderberg_stn]: ../literature/papers/Jaderberg_Spatial_Transformer_Networks.pdf
[yan_adawadsdfssoua]: ../literature/papers/Yan_Adversarial_Domain_Adaptation_with_a_Domain_Similarity_Discriminator_for_Semantic_Segmentation_of_Urban_Areas.pdf
[dosovitskiy_flofwcn]: ../literature/papers/Dosovitskiy_FlowNet_Learning_Optical_Flow_with_Convolutional_Networks.pdf
[fragkiadaki_ltsmoiv]: ../literature/papers/Fragkiadaki_Learning_to_Segment_Moving_Objects_in_Videos.pdf
[ganin_dtonn]: ../literature/papers/Ganin_Domain-Adversarial_Training_of_Neural_Networks.pdf
[mayer_aldttcnfdofasfe]: ../literature/papers/Mayer_A_Large_Dataset_to_Train_Convolutional_Networks_for_Disparity_Optical_Flow_and_Scene_Flow_Estimation.pdf
[yoon_pmfvosucnn]: ../literature/papers/Yoon_Pixel-Level_Matching_for_Video_Object_Segmentation_using_Convolutional_Neural_Networks.pdf
[ren_csmflusi]: ../literature/papers/Ren_Cross-Domain_Self-supervised_Multi-task_Feature_Learning_using_Synthetic_Imagery.pdf
[richter_pfdgtfcg]: ../literature/papers/Richter_Playing_for_Data_Ground_Truth_from_Computer_Games.pdf
[vandijk_hdnnsdisi]: ../literature/papers/van_Dijk_How_Do_Neural_Networks_See_Depth_in_Single_Images_ICCV_2019_paper.pdf
[godart_dissmdeiccv]: ../literature/papers/Godard_Digging_Into_Self-Supervised_Monocular_Depth_Estimation_ICCV_2019_paper.pdf
[hu_vocnnfmde]: ../literature/papers/Hu_Visualization_of_Convolutional_Neural_Networks_for_Monocular_Depth_Estimation_ICCV_2019_paper.pdf
[bozorgtabar_sdfafjlodem]: ../literature/papers/Bozorgtabar_SynDeMo_Synergistic_Deep_Feature_Alignment_for_Joint_Learning_of_Depth_ICCV_2019_paper.pdf
[vu_dadaiss]: ../literature/papers/Vu_DADA_Depth-Aware_Domain_Adaptation_in_Semantic_Segmentation_ICCV_2019_paper.pdf
[lee_spalfs]: ../literature/papers/Lee_SPIGAN_Privileged_Adversarial_Learning_from_Simulation.pdf
[gordon_dfvidw]: ../literature/papers/Gordon_Depth_From_Videos_in_the_Wild_Unsupervised_Monocular_Depth_Learning_ICCV_2019_paper.pdf
[ronneberger_ucnfbis]: ../literature/papers/Ronneberger_U-Net_Convolutional_Networks_for_Biomedical_Image_Segmentation.pdf
[he_drlfir]: ../literature/papers/He_Deep_Residual_Learning_for_Image_Recognition.pdf
[gaidon_vwapfmota]: ../literature/papers/Gaidon_Virtual_Worlds_as_Proxy_for_Multi-Object_Tracking_Analysis.pdf
[ros_tsdalcosifssous]: ../literature/papers/Ros_The_SYNTHIA_Dataset_CVPR_2016_paper.pdf
[cordts_tcdfsusu]: ../literature/papers/Cordts_The_Cityscapes_Dataset_for_Semantic_Urban_Scene_Understanding.pdf
[geiger_vmrtkd]: ../literature/papers/Geiger_Vision_meets_Robotics_The_KITTI_Dataset.pdf

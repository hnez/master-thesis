Gsusdn Report
=============

![Gsusdn network structure](presentations/final/diagrams/03_01_07_combined_network.png "Overall network structure of Gsusdn")

This repository contains a thesis and presentations
that describe the Gsusdn neural network that performs joint
supervised semantic segmentation and self-supervised depth
estimation training.

Self-supervised depth estimation
--------------------------------

Self-supervised depth estimation uses consecutive frames from
video footage to train a neural network to perform depth
estimation on single images.

![Self-supervised learning setup](presentations/final/diagrams/02_02_04_train_two_frames.png "Self-supervised learning setup")

Supervised semantic segmentation
--------------------------------

![Semantic segmentation example](presentations/final/diagrams/02_03_annotated.png "Semantic segmentation example")

A supervised semantic segmentation training uses labeled
input/output images for training.

![Supervised learning setup](presentations/final/diagrams/02_03_supervised_segmentation.png "Supervised learning setup")

Joint training
--------------

Joint training on video data from one domain and segmentation
data from another allows for better domain adaptation of
the semantic segmentation estimates on the video datasets.
Experiments were performed for adaptation from real camera
based datasets to other real camera based datasets but also
from synthetic datasets generated using computer graphics
to real datasets.

![Synthetic domain transfers](presentations/final/diagrams/04_03_01_summary.png "Synthetic domain transfers")

Build instructions
------------------

The source files for the thesis are contained in the
`print/` folder. Various dependencies are needed to
generate an actual .pdf from these sources and view it.
Try the following commands and hope for the best:

```
$ cd print
$ make
$ xdg-open print.pdf
```

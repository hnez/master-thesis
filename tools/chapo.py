#!/usr/bin/env python3

import re
import os
import sys

class Scanner(object):
    def __init__(self, bases):
        self.files = list()

        for base in bases:
            for (dirpath, _, filenames) in os.walk(base):
                for filename in filenames:
                    if any(filename.endswith(ext) for ext in self.EXTENSIONS):
                        self.files.append(os.path.join(dirpath, filename))

class Chapters(Scanner):
    EXTENSIONS = ('.tex',)

    def grep(self, text):
        refs = list()

        for fle in self.files:
            with open(fle) as fd:
                content = fd.read()

            if text in content:
                refs.append(fle)

        return refs

    def sed(self, renames):
        for fle in self.files:
            try:
                with open(fle) as fd:
                    content = fd.read()

                orig = content
            
                for src, dst in renames:
                    content = content.replace(src, dst)

                if orig != content:
                    print(f'  - {fle}')
                        
                    with open(fle, 'w') as fd:
                        fd.write(content)

            except FileNotFoundError:
                pass

class Diagrams(Scanner):
    EXTENSIONS = ('.tex',)

    def names(self):
        return list(f[:-4] for f in self.files)


class Images(Scanner):
    EXTENSIONS = ('.png', '.jpg')

    def names(self):
        return self.files


def main(cmd):
    c1 = Chapters(('chapters/',))
    c2 = Chapters(('chapters/', 'diagrams/'))
    ds = Diagrams(('diagrams/',))
    im = Images(('images/',))

    no_refs = list()
    refs = list()
    multi_refs = list()

    for d in ds.names():
        ref = c1.grep(d)

        if len(ref) == 0:
            no_refs.append(d)

        elif len(ref) == 1:
            refs.append((d, ref[0]))

        else:
            multi_refs.append(d)


    for i in im.names():
        ref = c2.grep(i)

        if len(ref) == 0:
            no_refs.append(i)

        elif len(ref) == 1:
            refs.append((i, ref[0]))

        else:
            multi_refs.append(i)


    chapter_missing = list()
    chapter_wrong = list()

    for dst, src in refs:
        src_match = re.search(r'(?P<chapter>[0-9]{2}(_[0-9]{2})*)_', src)

        if src_match is None:
            print(f' - {src} ignores naming convention')

        chapter = src_match[0]

        if (chapter in dst) or ('multi_' in dst):
            continue

        dst_match = re.search(r'(?P<chapter>[0-9]{2}(_[0-9]{2})*)_', dst)

        if dst_match is None:
            if chapter not in dst:
                chapter_missing.append((chapter, dst))

        else:
            pre = dst[:dst_match.start(0)]
            post = dst[dst_match.end(0):]
            should = pre + chapter + post

            chapter_wrong.append((dst, should))

    print('- Wrongly named multi refs:')

    for multi in multi_refs:
        if 'multi_' not in multi:
            print(f'  - {multi} does not contain "multi_"')

    print('- Missing chapter marks:')

    for chapter, dst in chapter_missing:
        print(f'  - {chapter} not in {dst}')

    renames = list()

    print('- Wrong chapter marks:')

    for dst, should in chapter_wrong:
        if '.' not in dst:
            print(f'mv {dst}.tex {should}.tex')
        else:
            print(f'mv {dst} {should}')

    print('- Not referenced:')

    for nr in no_refs:
        print(f'rm {nr}')

if __name__ == '__main__':
    cmd = sys.argv[1] if (len(sys.argv) > 1) else ''

    main(cmd)

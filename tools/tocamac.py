#!/usr/bin/env python3

import re
import os

class TOC(object):
    def __init__(self, path):
        with open(path) as fd:
            toc = fd.read()

        self.sections = dict()

        for match in re.finditer(r'\{\\numberline \{([\.\w]+)\}([\.\w\-/, ]+)\}', toc):
            number, name = match.groups()

            i = 1

            while (name, i) in self.sections.values():
                i += 1

            self.sections[number] = (name, i)

    def file_name(self, number):
        name = self.sections[number][0]

        nc = '_'.join(
            f'{int(n):02}'
            for n in number.split('.')
        )

        nn = name.lower()

        for bad, good in (('/', '_'), (' ', '_'), (',', ''), ('/', '_'), ('-', '_')):
            nn = nn.replace(bad, good)

        fn = nc + '_' + nn

        while '__' in fn:
            fn = fn.replace('__', '_')

        return fn

class PrintTex(object):
    def __init__(self, path):
        with open(path) as fd:
            self.ptx = fd.read()

    def replace_filename(self, section, filename):
        p0 = -1

        for i in range(section[1]):
            p0 += 1
            p0 = p0 + self.ptx[p0:].find('apter}{' + section[0] + '}')

        if p0 < 0:
            raise Exception(f'Broken section {section}')

        p1 = p0 + self.ptx[p0:].find('chapters/')

        if p1 < p0:
            raise Exception(f'Broken section {section}')

        p1 += len('chapters/')

        p2 = p1 + self.ptx[p1:].find(r'}')

        if p2 < p1:
            raise Exception(f'Broken section {section}')

        name_prev = self.ptx[p1:p2]
        self.ptx = self.ptx[:p1] + filename + self.ptx[p2:]

        return name_prev

    def save(self, path):
        with open(path, 'w') as fd:
            fd.write(self.ptx)

def move_chapter(old_name, new_name):
    old_path = os.path.join('chapters', f'{old_name}.tex')
    new_path = os.path.join('chapters', f'{new_name}.tex')

    print(f'{old_path} -> {new_path}')
    os.rename(old_path, new_path)

def main():
    toc = TOC('latex.out/print.toc')
    ptx = PrintTex('print.tex')

    for number, name in toc.sections.items():
        new_name = toc.file_name(number)
        old_name = ptx.replace_filename(name, new_name)

        if old_name != new_name:
            move_chapter(old_name, new_name)

    ptx.save('print.tex')

if __name__ == '__main__':
    main()

#!/usr/bin/env python3

import sys
from collections import defaultdict

TASK_REMAP = {
    'evalcs' : 'Cityscapes',
    'evalkihr' : '     KITTI'
}

DS_REMAP = {
    'cs' : '  Cityscapes',
    'c2' : '  CS Skipped',
    'ki' : '       KITTI',
    'k2' : 'KITTI Stereo',
    'g5' : '       GTA-V',
    'sy' : '     SYNTHIA',
    'vk' : ' Virt. KITTI',
    'no' : '           -'
}

def parse_slurms(paths):
    content = defaultdict(dict)

    for slurm_path in paths:
        with open(slurm_path) as fd:
            for ln in fd:
                elems = tuple(e.strip() for e in ln.split('|'))

                if (len(elems) == 5) and (elems[0] != 'eval_name'):
                    content[elems[0]][elems[2]] = elems[3]

    return content

def print_table(data):
    for row, vals in data.items():
        rs, net, gde, gse, gdo, dse, dd1, dd2, tsk = row.split('_')

        net = net[2:]
        gde, gse, gdo = tuple(g[0] + '.' + g[1:] for g in (gde, gse, gdo))
        dse, dd1, dd2 = tuple(DS_REMAP[d] for d in (dse, dd1, dd2))
        tsk = TASK_REMAP[tsk]

        none, dada_16, dada_13, dada_7, gio_10 = tuple(
            vals[k].replace('.', '').lstrip('0')[:-1] + '.' + vals[k][-1] + '\\%'
            for k in ('none', 'dada_16', 'dada_13', 'dada_7', 'gio_10')
        )

        table = ' & '.join((
            dse, dd1, dd2, rs, net, gde, gse, gdo,
            tsk, none, dada_16, dada_13, dada_7, gio_10
        ))

        print(table)

def main(argv):
    global db

    db = parse_slurms(argv[1:])
    print_table(db)

if __name__ == '__main__':
    main(sys.argv)

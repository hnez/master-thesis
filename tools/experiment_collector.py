#!/usr/bin/env python3

import sys
import os
import json

class Experiment(object):
    SIZES = {
        640 : 'lr',
        1280 : 'hr'
    }
    KI_RESIZES = {
        640 : (960, 288),
        1280 : (1920, 576)
    }
    CS_RESIZES = {
        640 : (1024, 512),
        1280 : (2048, 1024)
    }
    NETS = {
        18 : 'rn18',
        50 : 'rn50'
    }
    DEPTH_DS = {
        '' : 'no_no',
        'cityscapes_sequence_train' : 'cs_no',
        'kitti_kitti_train' : 'ki_no',
        'kitti_kitti_train,cityscapes_sequence_train' : 'ki_cs',
        'kitti_kitti_train,cityscapes_sequence_skip_train' : 'ki_c2',
        'kitti_kitti_stereo_train' : 'k2_no'
    }
    SEG_DS = {
        'cityscapes_train' : 'cs',
        'gta5_train' : 'g5',
        'synthia_train' : 'sy',
        'virtual_kitti_train' : 'vk'
    }

    def __init__(self, path):
        self.path = path

        self.opt = None
        self.model_path = None
        self.opt_path = None

        for de in os.scandir(self.path):
            opt_path = os.path.join(de.path, 'opt.json')
            model_path = os.path.join(de.path, *('checkpoints', 'epoch_20', 'model.pth'))

            try:
                with open(opt_path) as fd:
                    self.opt = json.loads(fd.read())
                    self.opt_path = opt_path

                with open(model_path) as fd:
                    self.model_path = model_path
            except:
                continue

        if self.opt is None:
            print(f'Missing opt.json in {self.path}')
            raise Exception()

        if self.model_path is None:
            print(f'Missing model file in {self.path}')
            raise Exception()

    def _dir_name(self):
        siz = self.SIZES[self.opt['depth_resize_width']]
        nes = self.NETS[self.opt['model_num_layers']]

        gde = self.opt['train_depth_grad_scale']
        gse = self.opt['train_segmentation_grad_scale']
        gdm = -self.opt['train_domain_grad_scale'] if ('train_domain_grad_scale' in self.opt) else 0

        dds = self.DEPTH_DS[self.opt['depth_training_loaders']]
        sds = self.SEG_DS[self.opt['segmentation_training_loaders']]

        name = f'{siz}_{nes}_{gde:1.3f}_{gse:1.3f}_{gdm:1.3f}_{sds}_{dds}'
        name = name.replace('.', '')

        return name

    def print_cp_cmds(self):
        print(f'### Experiment Dir {self.path}:')

        dn = self._dir_name()

        print(f'mkdir {dn}')
        print(f'cp {self.opt_path} {dn}')
        print(f'cp {self.model_path} {dn}')
        print(f'echo "{self.path}" > {dn}/src_dir')
        print()

    def print_eval_cmds(self):
        print(f'### Experiment Dir {self.path}:')

        dn = self._dir_name()
        krsx, krsy = self.KI_RESIZES[self.opt['depth_resize_width']]
        crsx, crsy = self.CS_RESIZES[self.opt['depth_resize_width']]
        layers = self.opt['model_num_layers']

        print('echo', '"' + ''.join('-' for _ in dn) + '"')
        print(f'echo "{dn}"')
        print('echo', '"' + ''.join('-' for _ in dn) + '"')
        print()

        print('python3 eval_segmentation.py \\')
        print('  --sys-best-effort-determinism \\')
        print(f'  --model-num-layers {layers} \\')
        print(f'  --model-name "{dn}_evalcs" \\')
        print('  --eval-segmentation-remaps "none,dada_16,dada_13,dada_7,gio_10" \\')
        print('  --segmentation-validation-loaders "cityscapes_validation" \\')
        print(f'  --segmentation-resize-width {crsx} \\')
        print(f'  --segmentation-resize-height {crsy} \\')
        print(f'  --segmentation-validation-crop-width {crsx} \\')
        print(f'  --segmentation-validation-crop-height {crsy} \\')
        print(f'  --model-load {dn}')
        print()

        print('python3 eval_segmentation.py \\')
        print('  --sys-best-effort-determinism \\')
        print(f'  --model-num-layers {layers} \\')
        print(f'  --model-name "{dn}_evalkihr" \\')
        print('  --eval-segmentation-remaps "none,dada_16,dada_13,dada_7,gio_10" \\')
        print('  --segmentation-validation-loaders "kitti_2015_train" \\')
        print(f'  --segmentation-resize-width {krsx} \\')
        print(f'  --segmentation-resize-height {krsy} \\')
        print(f'  --segmentation-validation-crop-width {krsx} \\')
        print(f'  --segmentation-validation-crop-height {krsy} \\')
        print(f'  --model-load {dn}')
        print()
        print()


class CheckpointDir(object):
    def __init__(self, path):
        self.path = path

        experiments = list()

        for de in os.scandir(self.path):
            if not de.is_dir():
                continue

            try:
                ex = Experiment(de.path)
                experiments.append(ex)

            except:
                print(f'Scanning {de.path} failed with exception!')

        self.experiments = tuple(experiments)

    def print_cp_cmds(self):
        print(f'## Checkpoint Dir {self.path}:')

        for ex in self.experiments:
            ex.print_cp_cmds()

    def print_eval_cmds(self):
        print(f'## Checkpoint Dir {self.path}:')

        for ex in self.experiments:
            ex.print_eval_cmds()


def main(argv):
    global checkpoint_dirs
    checkpoint_dirs = tuple(
        CheckpointDir(path) for path in argv[1:]
    )

    print("#!/bin/bash")
    print()
    print("# Copy commands:")

    for cd in checkpoint_dirs:
        cd.print_cp_cmds()

    print("#!/bin/bash")
    print()
    print("# Eval commands:")

    for cd in checkpoint_dirs:
        cd.print_eval_cmds()

if __name__ == '__main__':
    main(sys.argv)

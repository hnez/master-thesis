#!/usr/bin/env python3

import os
import sys
from PIL import Image

def resize_img(path):
    print(f'{path[-50:]} ...', end='')
    img = Image.open(path)

    orig_x = img.size[0]
    orig_y = img.size[1] // 3

    new_x = 500
    new_y = new_x * orig_y // orig_x

    top, mid, bot = tuple(
        img.resize(
            (new_x, new_y), Image.BICUBIC,
            (0, orig_y * i, orig_x, orig_y * (i + 1))
        )
        for i in range(3)
    )

    path = path.replace('.png', '.jpg')
    meh, muh = os.path.split(path)

    top.save(os.path.join(meh, 'top_' + muh))
    mid.save(os.path.join(meh, 'mid_' + muh))
    bot.save(os.path.join(meh, 'bot_' + muh))

    print('done')

for name in sys.stdin:
    resize_img(name.strip())

import torch
import torch.nn as nn

class ReflectionPad2d(nn.ReflectionPad2d):
    def forward(self, *kargs, **kwargs):
        print(f'[Reflection Pad]')

        return super().forward(*kargs, **kwargs)


class Conv2d(nn.Conv2d):
    def forward(self, *kargs, **kwargs):
        print(f'|-[{self.in_channels}]')
        print(f'[Conv2D(kernel_size={self.kernel_size[0]}, stride={self.stride[0]})]')
        print(f'|-[{self.out_channels}]')

        return super().forward(*kargs, **kwargs)


class ELU(nn.ELU):
    def forward(self, *kargs, **kwargs):
        print(f'[ELU]')

        return super().forward(*kargs, **kwargs)


class Upsample(nn.Upsample):
    def forward(self, x, **kwargs):
        print(f'[Upsample {x.shape[2]} -> {2 * x.shape[2]}]')

        return super().forward(x, **kwargs)


class PreConvBlock(nn.Module):
    """Decoder basic block
    """

    def __init__(self, pos, n_in, n_out):
        super().__init__()

        self.pos = pos

        self.pad = ReflectionPad2d(1)
        self.conv = Conv2d(n_in, n_out, 3)
        self.nl = ELU()

    def forward(self, *x):
        x_pre = x[:self.pos]
        x_cur = x[self.pos]
        x_pst = x[self.pos+1:]

        print(f'- PreConv {self.pos}')

        x_cur = self.pad(x_cur)
        x_cur = self.conv(x_cur)
        x_cur = self.nl(x_cur)

        return x_pre + (x_cur, ) + x_pst


class UpSkipBlock(nn.Module):
    """Decoder basic block

    Perform the following actions:
        - Upsample by factor 2
        - Concatenate skip connections (if any)
        - Convolve
    """

    def __init__(self, pos, ch_in, ch_skip, ch_out):
        super().__init__()

        self.pos = pos

        self.up = Upsample(scale_factor=2)

        self.pad = ReflectionPad2d(1)
        self.conv = Conv2d(ch_in + ch_skip, ch_out, 3)
        self.nl = ELU()

    def forward(self, *x):
        x_pre = x[:self.pos]
        x_skp = x[self.pos:self.pos+1]
        x_pst = x[self.pos+1:]

        print(f'- UpSkip {self.pos}')

        # up-scale input channels
        x_new = self.up(x_pre[-1])

        # Mix in skip connections from the encoder
        # (if there are any)
        if len(x_skp) > 0:
            print(f'[Concat {x_new.shape} and {x_skp[0].shape}]')
            x_new = torch.cat((x_new, x_skp[0]), 1)

        # Combine up-scaled input and skip connections
        x_new = self.pad(x_new)
        x_new = self.conv(x_new)
        x_new = self.nl(x_new)

        return x_pre + (x_new, ) + x_pst


class PartialDecoder(nn.Module):
    """Decode some features encoded by a feature extractor

    Args:
        chs_dec: A list of decoder-internal channel counts
        chs_enc: A list of channel counts that we get from the encoder
        start: The first step of the decoding process this decoder should perform
        end: The last step of the decoding process this decoder should perform
    """

    def __init__(self, chs_dec, chs_enc, start=0, end=None):
        super().__init__()

        self.start = start
        self.end = (2 * len(chs_dec)) if (end is None) else end

        self.chs_dec = tuple(chs_dec)
        self.chs_enc = tuple(chs_enc)

        self.blocks = nn.ModuleDict()

        for step in range(self.start, self.end):
            macro_step = step // 2
            mini_step = step % 2
            pos_x = (step + 1) // 2

            # The decoder steps are interleaved ...
            if (mini_step == 0):
                n_in = self.chs_dec[macro_step - 1] if (macro_step > 0) else self.chs_enc[0]
                n_out = self.chs_dec[macro_step]

                # ... first there is a pre-convolution ...
                self.blocks[f'step_{step}'] = PreConvBlock(pos_x, n_in, n_out)

            else:
                # ... and then an upsampling and convolution with
                # the skip connections input.
                n_in = self.chs_dec[macro_step]
                n_skips = self.chs_enc[macro_step + 1] if ((macro_step + 1) < len(chs_enc)) else 0
                n_out = self.chs_dec[macro_step]

                self.blocks[f'step_{step}'] = UpSkipBlock(pos_x, n_in, n_skips, n_out)

    def chs_x(self):
        return self.chs_dec + self.chs_dec[-1:]

    @classmethod
    def gen_head(cls, chs_dec, chs_enc, end=None):
        return cls(chs_dec, chs_enc, 0, end)

    @classmethod
    def gen_tail(cls, head, end=None):
        return cls(head.chs_dec, head.chs_enc, head.end, end)

    def forward(self, *x):
        for step in range(self.start, self.end):
            x = self.blocks[f'step_{step}'](*x)

        return x


class MultiRes(nn.Module):
    """ Directly generate target-space outputs from (intermediate) decoder layer outputs
    Args:
        dec_chs: A list of decoder output channel counts
        out_chs: output channels to generate
        pp: A function to call on any output tensor
            for post-processing (like e.g. a non linear activation)
    """

    def __init__(self, dec_chs, out_chs, pp=None):
        super().__init__()

        self.pad = ReflectionPad2d(1)

        self.convs = nn.ModuleList(
            Conv2d(in_chs, out_chs, 3)
            for in_chs in dec_chs
        )

        self.pp = pp if (pp is not None) else self._identity_pp

    def _identity_pp(self, x):
        return x

    def forward(self, *x):
        print('- Multires')
        out = tuple(
            self.pp(conv(self.pad(inp)))
            for conv, inp in zip(self.convs, x)
        )

        return out


class MultiResDepth(MultiRes):
    def __init__(self, dec_chs, out_chs=1):
        super().__init__(dec_chs, out_chs, nn.Sigmoid())

        # Just like in the PoseDecoder, where outputting
        # large translations at the beginning of training
        # is harmful for stability outputting large depths
        # at the beginning of training is a source
        # of instability as well. Increasing the bias on
        # the disparity output decreases the depth output.
        for conv in self.convs:
            conv.bias.data += 5


class MultiResSegmentation(MultiRes):
    def __init__(self, dec_chs, out_chs=20):
        super().__init__(dec_chs, out_chs)


class MultiResDomain(MultiRes):
    def __init__(self, dec_chs, out_chs=3):
        super().__init__(dec_chs, out_chs)


pd = PartialDecoder((256, 128, 64, 32, 16), (512, 256, 128, 64, 64))
mr = MultiResSegmentation(pd.chs_x()[-4:])

l0 = torch.randn(1, 64, 32, 32)
l1 = torch.randn(1, 64, 16, 16)
l2 = torch.randn(1, 128, 8, 8)
l3 = torch.randn(1, 256, 4, 4)
l4 = torch.randn(1, 512, 2, 2)

x = pd(l4, l3, l2, l1, l0)
mr(*x[-4:])

The \shdski{} dataset was recorded using a car-mounted sensor platform
in and around the city of Karlsruhe, Germany \cite{geiger_2013_kitti}.
The dataset contains data from two grayscale cameras, two color
cameras, a 3D-\acrshort{lidar} scanner and \acrshort{gps}/inertial measurement sensors.
Our experiments make use of images from both color cameras for
training purposes and the \acrshort{lidar} data as ground truth in depth estimation
evaluation.

For depth training a triplet of image frames are used, one
to peform depth estimation on and two as reference frames for
image reconstruction. An example of such a triplet is shown
in \autoref{img:kitti_sequence}.
We use a subset of the dataset introduced by Godard~\etal~\cite{godard_2017_monodepth},
called KITTI Split, where we treat the images from the left and right
color camera as separate datasets and use all samples for which
previous and next frames are available.
The number of available triplets, and other dataset specific
data is shown in \autoref{table:kitti_depth_train_data}.

\figurizethreegraphics{images/multi_previews/kitti-kitti-train-depth/8-color-prev.png}
                      {images/multi_previews/kitti-kitti-train-depth/8-color-cur.png}
                      {images/multi_previews/kitti-kitti-train-depth/8-color-next.png}
                      {img:kitti_sequence}
                      {A \shdski{} frame with previous/next context}
                      {A \shdski{} frame with previous/next context}
                      {H}

\begin{table}[H]
  \centering
  \begin{tabular}{c | c | c}
    Samples & Resolution        & Framerate\\ \hline
    $57874$ & $1226 \times 370$ & $\SI{10}{\hertz}$
  \end{tabular}

  \caption{\shdski{} (KITTI Split) depth training datasheet}
  \label{table:kitti_depth_train_data}
\end{table}

The KITTI split also sets aside images for model validation
and testing. The validation set consists of images from the
left color camera and corresponding \acrshort{lidar} scans, that were
removed from the training set.
The \acrshort{lidar} sensor only produces a small number of depth
measurments, when compared to the camera's resolution.
An example of a camera image and depth ground truth
pair is shown in \autoref{img:kitti_val_depth}.
A summary of the validation set images and the percentage
of pixels with corresponding depth information is shown
in \autoref{table:kitti_depth_validation_data}.

\figurizetwographics{images/multi_previews/kitti-kitti-val-depth/11-color.png}
                    {images/multi_previews/kitti-kitti-val-depth/11-depth.png}
                    {img:kitti_val_depth}
                    {A \shdski{} frame with depth ground truth}
                    {A \shdski{} frame with depth ground truth}
                    {H}

\begin{table}[H]
  \centering
  \begin{tabular}{c | c | c}
    Samples & Resolution        & Annotations \\ \hline
    $1159$  & $1226 \times 370$ & 4.26\%
  \end{tabular}

  \caption{\shdski{} (KITTI Split) depth validation datasheet}
  \label{table:kitti_depth_validation_data}
\end{table}

For testing purposes a small subset of the \shdski{} dataset
is used for which more dense depth information and hand-annotated
semantic segmentation labels are available.
An example frame is shown in \autoref{img:kitti_2015_test}.
A summary of the depth evaluation portion of this
subset if shown in \autoref{table:kitti_depth_test_data}.
\autoref{table:kitti_segmentation_validation_data}
shows a breakdown of the percentage of pixels annotated as a
given class.

\figurizethreegraphics{images/multi_previews/kitti-2015-val-seg/10-segmentation.png}
                      {images/multi_previews/kitti-2015-train-depth/10-color.png}
                      {images/multi_previews/kitti-2015-train-depth/10-depth.png}
                      {img:kitti_2015_test}
                      {A \shdskv{} frame with segmentation and depth ground truth}
                      {A \shdskv{} frame with segmentation and depth ground truth}
                      {H}

\begin{table}[H]
  \centering
  \begin{tabular}{c | c | c}
    Samples & Resolution        & Annotations \\ \hline
    $200$   & $1226 \times 370$ & 19.73\%
  \end{tabular}

  \caption{\shdskv{} depth testing datasheet}
  \label{table:kitti_depth_test_data}
\end{table}

\begin{table}[H]
  \centering
  \begin{tabular}{c | c}
    Samples & Resolution \\ \hline
    $200$   & $1226 \times 370$
  \end{tabular}

  \begin{tabular}{c | c | c | c | c | c | c}
    Road    & Sidewalk & Building & Wall   & Fence  & Pole   & Traffic Light \\ \hline
    23.10\% & 3.82\%   & 8.16\%   & 0.92\% & 0.83\% & 1.36\% & 0.31\%
  \end{tabular}

  \begin{tabular}{c | c | c | c | c | c | c}
    Traffic Sign & Vegetation & Terrain  & Sky     & Person  & Rider   & Car \\ \hline
    0.55\%       & 30.32\%    & 9.42\%   & 10.67\% &  0.09\% & 0.03\%  & 6.01\%
  \end{tabular}

  \begin{tabular}{c | c | c | c | c | c}
    Truck  & Bus    & Train  & Motorcycle & Bicycle & None  \\ \hline
    0.22\% & 0.07\% & 0.21\% & 0.01\%     & 0.06\%  & 3.84\%
  \end{tabular}

  \caption{\shdskv{} segmentation validation datasheet}
  \label{table:kitti_segmentation_validation_data}
\end{table}

Using a self-supervised approach it is possible to
train a neural network to perform the depth estimation
task, as discussed above, without the need for ground truth
depth information at all, instead relying on consecutive frames
of video.

While the training process uses multiple images and
sub-networks, it is important to note that the trained
network model is used in the same way as a network
trained in a supervised fashion, e.g. as shown
in \autoref{img:unsupervised_eval}.

\figurizefile{diagrams/02_02_04_eval_mode}
             {img:unsupervised_eval}
             {The input and output shapes of the depth estimation network
               do not depend on the training method (self-supervised vs. supervised).}
             {Inputs and outputs of a network trained using self-supervision}
             {0.9}{ht}

A training setup that uses two consecutive frames for self-supervised
depth estimation training is shown in \autoref{img:unsupervised_2f}.
The training setup consists of two neural networks, containing
trainable parameters, and other parameter-less building blocks.
The two networks expect different inputs and produce different
outputs:

\begin{description}
  \item[Depth-Net] The depth estimation network inputs,
  outputs and structure are identical to the previously-discussed
  depth-estimation networks trained in a supervised fashion.
  It expects a color image as input and produces a
  depth map of equal size as an output.

  \item[Pose-Net] The pose estimation network expects two
  images as input and produces a vector of six elements
  as output.
  The output values are an estimate of the
  (\gls{sym:x},\gls{sym:y},\gls{sym:z})-translation
  and rotation about the
  (\gls{sym:x},\gls{sym:y},\gls{sym:z})-axis of the camera pose
  that happened between the capture of both input images.
\end{description}

The training objective is to improve the reconstruction of
a \textit{target} image from a \textit{source} image,
based on these depth and pose estimates.

\figurizefile{diagrams/02_02_04_train_two_frames}
             {img:unsupervised_2f}
             {While the depth estimation inputs and output are independent
               of the training method, the overall training setup is
               more involved in case of self-supervised depth estimation training,
               as it uses multiple input images.}
             {Unsupervised training setup using two consecutve frames}
             {0.9}{ht}

The first step of the reconstruction is a inverse camera
projection \circnum{1} from depth estimates to a pointcloud,
based on known camera parameters.
Two views of an exemplary pointcloud, are shown in
Figures \ref{img:pointcloud_back} and \ref{img:pointcloud_side}.
These views of the pointcloud are projections onto images
planes that are not the camera's image plane.

\begin{minipage}[t]{.5\textwidth}
  \noindent
  \figurizegraphic{images/02_02_04_pointcloud1.png}
                  {img:pointcloud_back}
                  {Pointcloud (back view)}
                  {A reconstrunted pointcloud (viewed from the back)}
                  {0.95}{H}
\end{minipage}
\begin{minipage}[t]{.5\textwidth}
  \noindent
  \figurizegraphic{images/02_02_04_pointcloud2.png}
                  {img:pointcloud_side}
                  {Pointcloud (side view)}
                  {A reconstrunted pointcloud (viewed from the side)}
                  {0.95}{H}
\end{minipage} \\

In the coordinate transformation step \circnum{2},
the 3D-coordinates of the pointcloud-points are
transformed from the coordinate system of the
\textit{target} frame to the coordinate system of
the \textit{source} frame, by using the estimated
pose change between these frames.

Under the assumption that the spectator's camera
is the only moving object in the scene, the points
in both coordinate systems depict the same part
of the same object and should thus be of the same color.
The next steps use this assumption to reconstruct the
\textit{target} image using only color information from
the \textit{source} image. \\

In the projection step \circnum{3} the 3D point coordinates
in the coordinate system of the \textit{source} frame are
projected into 2D coordinates in the image plane of
the \textit{source} camera.

The result of this reprojection procedure is a mapping
that maps the discrete pixel coordinates of the \textit{target}
image into continuous pixel coordinates in the \textit{source}
image that depict the same point in space.

In the sampling step \circnum{4} the color information
for these points is sampled from the \textit{source} frame
to reconstruct the \textit{target} image.

The training objective \circnum{5} is to minimize the difference between
the reconstructed and actual \textit{target} image.
As the quality of reconstruction is dependent on good
depth and pose estimates, these are optimized as a side-effect. \\

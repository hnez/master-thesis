Image classification produces a global output estimate
over an input image, thus the input image with
two spatial dimensions is reduced to an output of zero
spatial dimensions.

An alternative image processing task is to preserve the spatial dimension
of the input in the output, e.g. generate a different
kind of output image from an input image.
A simplified schematic of such a network,
based on the structure of U-Net \cite{ronneberger_2015_unet}
is shown in \autoref{img:fcn_unet}.
This structure utilizes so-called skip connections
to both enable the network to incorperate features
from large regions of the input image, while using
small convolution kernel sizes and also
allowing the output to have a high spatial resolution.

The network does not incorporate any fully-connected
layers, instead relying only on convolutions, pooling and
upsampling. Such a structure is accordingly named a \acrfull{fc_cnn}.

\figurizefile{diagrams/02_01_04_fcn}
             {img:fcn_unet}
             {A \acrshort{fc_cnn} is a \acrshort{cnn} that does not include any
             fully-connected layers. The left half of the diagram contains an
             encoder network structure that combines convolutional layers
             and pooling to successively reduce the feature dimensions.
             The right half of the diagram contains a decoder network structure
             that sucessively increases the feature dimensions to match the
             initial resolution.
             A fully convolutional network structure like this is able to
             perform image-to-image transformation tasks.}
             {Structure of a \acrlong{fc_cnn}}
             {0.9}{ht}

The left half of the network is commonly called
an encoder, feature extractor or backbone network,
while the right half is commonly called a decoder network.
The encoder network is composed of multiple blocks
of convolutions, non-linearities and pooling layers
that successively reduce the spatial image resolution
while at the same time increasing the number
of ``color`` channels.
The decoder network consists of layers accepting
two inputs each, one of them being of higher spatial
resolution the other having more channels.
The former allow the network to produce output of high
spatial resolution, while the latter allows the network
to consider a larger region of the input image.
The higher resolution inputs are commonly called skip
connections, as they skip over parts of the encoder
and decoder network.

The two inputs to the decoder layers are combined by
upsamling the lower resolution image to match the resolution
of the higher resolution image and then concatenating
them along their ``color``-channel axis.

The concatenated images are then convolved, and processed by
an activation function. \\

\pagebreak

The operations in the encoder and decoder are thus
(note that upsamling can be considered a counterpart
to pooling): \\

\noindent
\begin{minipage}[t]{.5\textwidth}
  \underline{Encoder}

  \begin{itemize}
    \item Convolve
    \item Activation function
    \item Pooling
  \end{itemize}
\end{minipage}
\begin{minipage}[t]{.5\textwidth}
  \underline{Decoder}

  \begin{itemize}
    \item Upsampling
    \item Concatenation of skip-connections
    \item Convolve
    \item Activation function
  \end{itemize}
\end{minipage}

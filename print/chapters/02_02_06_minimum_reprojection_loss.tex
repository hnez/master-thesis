The training setup in \autoref{img:unsupervised_2f} is based on
a number of assumtions, concerning the image composition and
the way they are recorded, that do not always hold in reality.
Some of these failed assumptions and possible mitigations
are demonstrated in \autoref{img:failed_assumptions} and
are discussed in this section.
Shown in \autoref{img:failed_assumptions} is the \textit{target}
image as it should have been reproduced and the \textit{reprojection}
that is actually generated from the \textit{source} image.

\figurizefile{diagrams/02_02_06_three_horsemen}
             {img:failed_assumptions}
             {Effects of out-of-region pixels, occlusion and moving objects on the reprojection quality.
               Out-of-region pixels result ins smeared image borders, occlusions result in duplicates and
               moving objects result in general distortion}
             {Effects of out-of-region pixels, occlusion and moving objects on reprojection}
             {0.9}{ht}

\begin{description}
  \item[Out-Of-Region Pixels] Due to the change in viewport some
  parts of the scene are only visible in either the \textit{source}
  or \textit{target} image.
  Making reconstruction of the other image from the missing
  data impossible. The color information is instead sampled
  from the nearest pixels at the image border.

  \item[Occlusion] Parts of the scene may be occluded by objects
  in either the \textit{source} or \textit{target} image, but not both.
  This results in duplicates in the \textit{reprojection}.

  \item[Moving Objects] The reprojection process assumes, that
  a point in space is either transparent, or has a constant color.
  This assumption is broken by moving objects.
  This results in color information being sampled from wrong objects,
  resulting in duplicates and distortion.
\end{description}

\figurizefile{diagrams/02_02_06_min_reproj_loss}
             {img:min_reproj_loss}
             {A mitigation technique to the previously-mentioned adverse
               effects is the minimum-reprojection loss that combines
               multiple reprojected images}
             {Minimum reprojection loss in a training setup}
             {0.9}{ht}

Effects of out-of-region pixels and occlusion can mostly
be mitigated by using the minimum reprojection loss
introduced by Godard~\etal~\cite{godard_2019_monodepth}.
The general concept is presented in \autoref{img:min_reproj_loss}.
The same depth map for the \textit{target} image is used to
perform the reprojection not only from one \textit{source} image,
but from one recorded before the \textit{target} image
and one after.
For both reprojected images the per-pixel error to the
\textit{target} image is calculated and the per-pixel
minimum from \textit{source 1} and \textit{source 2}
is used to form the overall loss.

\figurizefile{diagrams/02_02_06_min_reproj_loss_recon}
             {img:min_reproj_loss_recon}
             {An example of the minimum reprojection loss.
               The color-coded image indicates from which of
               the reprojected images the corresponding pixel's
               loss is taken}
             {Example for the minimum reprojection loss}
             {0.9}{ht}

An example for this process is shown in
\autoref{img:min_reproj_loss_recon}, along with
a color-coded image of which image the per-pixel
loss is selected from. \\

Godard~\etal~\cite{godard_2019_monodepth} also introduced
the concept of auto-masking, a mitigation for objects
traveling at the same speed as
the observer's camera, which appear at the same position
in all images and would thus be learned to be at an
infinte distance.
In addition to the reprojected previous and next frame
also the original previous and next frame are added
as options to the minimum reprojection loss.
The minimum reprojection loss presents a hard per-pixel
choice from its four inputs, which means that the
loss from pixels where one of the original images was
selected is independent of the network parameters,
stopping backpropagation information from these pixels
from flowing back into the network.

\figurizefile{diagrams/02_02_06_automask}
             {img:automask}
             {When using auto-masking, in addition to the options shown in
               \autoref{img:min_reproj_loss_recon}, the minimum reprojection
               loss can also select from the un-warped source images}
             {Auto-masking as a mitigation for moving objects}
             {0.9}{H}

There are two main methods of generating depth estimates without
the use of specialized sensors, those ones based on multiple images
recorded from different camera poses, hereby called \textit{stereo}-Methods,
and those based only on a single camera image, called monocular
methods from the Greek \textit{mono} for single and the Latin \textit{ocular}
for eye \cite{wiki_xxxx_monocular}. \\

By using images from multiple camera poses it is possible
to pinpoint the 3D-location of objects by matching patterns
in the different images and deriving the locations from
geometric clues, using these matched patterns and prior
knowledge about the camera poses and parameters.
This process is shown in \autoref{img:stereo}, where the location of the four
highlighted points can be calculated using angles and
the distance between cameras. By matching a large
number of points, it is possible to reconstruct a relatively dense estimate
of the image depth. \\

\figurizefile{diagrams/02_02_02_stereo}
             {img:stereo}
             {Using pattern matching to identify the same 3D-points in two
               projections of a single scene it is possible to estimate the
               positions of said points using the translation and projection
               matrices \gls{sym:trans_matrix} and \gls{sym:proj_matrix}}
             {Depth estimation based on stereo imagery and pattern matching}
             {0.9}{ht}

The difference in position and rotation between the
camera poses can be modeled by a $4 \times 4$ Transformation
matrix, a combination of a rotation matrix $\gls{sym:trans_matrix}_\text{rot}$
and a translation matrix $\gls{sym:trans_matrix}_\text{trans}$.
By using the matched positions in projected coordinates $(\gls{sym:x}_{p,1}, \gls{sym:y}_{p,1})$
and $(\gls{sym:x}_{p,2}, \gls{sym:y}_{p,2})$ the known camera and transformation matrices
$\bm{P}$ and $\gls{sym:trans_matrix}$ and Equations \ref{eq:stereo_match_start}
- \ref{eq:stereo_match_end} the unkown depths $\gls{sym:homo_w}_1$ and $\gls{sym:homo_w}_2$ in
the respective camera's frame of reference can be calculated.

\vspace{0.5cm}
\begin{minipage}[H]{.45\textwidth}
  \begin{align}
    \begin{pmatrix}
      \gls{sym:x}_1 \\ \gls{sym:y}_1 \\ \gls{sym:z}_1
    \end{pmatrix}
    &= \bm{P}^{-1}
    \begin{pmatrix}
      \gls{sym:x}_{p,1} \\ \gls{sym:y}_{p,1} \\ \gls{sym:homo_w}_1
    \end{pmatrix}
    \label{eq:stereo_match_start} \\
    \begin{pmatrix}
      \gls{sym:x}_2 \\ \gls{sym:y}_2 \\ \gls{sym:z}_2
    \end{pmatrix}
    &= \bm{P}^{-1}
    \begin{pmatrix}
      \gls{sym:x}_{p,2} \\ \gls{sym:y}_{p,2} \\ \gls{sym:homo_w}_2
    \end{pmatrix}
  \end{align}
\end{minipage}
\hfill\vline\hfill
\begin{minipage}[H]{.45\textwidth}
  \begin{align}
    \gls{sym:trans_matrix} &= \gls{sym:trans_matrix}_\text{rot} \gls{sym:trans_matrix}_\text{trans} \\
    \begin{pmatrix}
      \gls{sym:x}_h \\ \gls{sym:y}_h \\ \gls{sym:z}_h \\ \gls{sym:homo_w}
    \end{pmatrix}
    & = \gls{sym:trans_matrix}
    \begin{pmatrix}
      \gls{sym:x}_1 \\ \gls{sym:y}_1 \\ \gls{sym:z}_1 \\ 1
    \end{pmatrix} \\
    \begin{pmatrix}
      \gls{sym:x}_2 \\ \gls{sym:y}_2 \\ \gls{sym:z}_2
    \end{pmatrix}
    &=
    \begin{pmatrix}
      \gls{sym:x}_h / \gls{sym:homo_w} \\ \gls{sym:y}_h / \gls{sym:homo_w}  \\ \gls{sym:z}_h / \gls{sym:homo_w}
    \end{pmatrix}
    \label{eq:stereo_match_end}
  \end{align}
\end{minipage}
\vspace{0.5cm}

When relying on monocular imagery, taken from a single
camera pose, these kinds of pattern matching and
usage of geometric considerations, are however not possible.

Instead one has to rely on prior knowledge about the shape
of depicted objects, in order to perform geometric considerations
based on these dimensions.
This is demonstrated in \autoref{img:mono}, where known dimensions
of objects are shown in blue and can be used as a base for
depth estimation.

Learning based approaches, like those based on \acrlongpl{cnn}
are able to learn this intuitive information about the
shape of depicted objects, and thus to learn depth estimation from
monocular imagery, given availability of sufficient training data.

\figurizefile{diagrams/02_02_02_mono}
             {img:mono}
             {Given a good estimate of the dimensions of objects in the scene
               it is possible to estimate their distances from only one projection
               of said scene}
             {Depth estimation using monocular imagery and prior knowledge}
             {0.9}{ht}

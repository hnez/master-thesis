Shown in \autoref{tab:baseline_cs} are evaluation results for neural network models
trained using conventional supervised semantic segmentation training,
when evaluated on the \shdscsv{}.
Training is performed on the \shdscs{}, \shdsgv{}, \shdssy{} and \shdsvk{} segmentation
datasets using the previously introduced low resolution ($640 \times 192$ pixel network inputs)
and \shrns{} based learning strategy. The \shdscs{} dataset was also used to
train networks using the high resolution ($1280 \times 384$ pixel network inputs)
and \shrnl{} based training strategies. \\

The segmentation quality of these network models is compared using
the \shmi{} over the $19$ classes in the \shevcs{} subset
of segmentation classes. \\

\tableizefile{tables/baseline_cs}
             {tab:baseline_cs}
             {Segmentation quality of network models trained solely on segmentation datasets
               when evaluated on the \shdscsv{}}
             {Baseline segmentation quality (\shdscsv{})}
             {1.0}{ht}

When comparing the results for network models trained on the \shdscs{} dataset
with those trained on the other, synthetic, datasets the effect of domain
shift becomes apparent.
The models trained and evaluated on the \shdscs{} dataset score an
\shmics{} consistently above $\SI{60}{\percent}$ the models
evaluated on a dataset dissimilar to the one they were trained on
score only an \shmics{} at around, or below, $\SI{30}{\percent}$.
When compared side-by-side these differences in segmentation quality are
also visually apparent, as shown in \autoref{img:baseline_cs}, where
exemplary evaluation output images are collected for each training
setup.\footnote{See \autoref{img:apx_real_evalcs} and \autoref{img:apx_synthetic_evalcs}
for a larger collection of example images.}\\

Somewhat surprisingly the best-performing model is the one
trained on \shdscs{} with both the low input image resolution
and smaller encoder network size, the training setup that requires
the least ressource usage during training and evaluation,
when compared to its high resolution and deeper-network counterparts.
Possible sources of these performance degradations are, in
both cases, badly-tuned hyperparameters like batch-size, when
compared to the smaller setup, or, in case of the deeper network
structure overfitting to the relatively few \shdscs{} training
samples. \\

\figurizefile{diagrams/04_01_baseline_evalcs}
             {img:baseline_cs}
             {Comparison of estimates from models trained using different segmentation-only
               training setups when evaluated on the \shdscsv{}.
               Please note, that the area of the engine hood is not labeled
               as belonging to a specific class and is thus ignored in
               the calculation of the \shmi{}.}
             {Comparision of baseline segmentation quality (\shdscsv{})}
             {0.9}{ht}

The networks trained on synthetic datasets, when viewed in isolation,
also show a great variance in evaluation quality.
The model trained on \shdsgv{} can benefit from this dataset's large number
of training samples, its high photorealism, realistic camera poses and
an availability of training samples for every class in the \shevcs
evaluation class subset and, while still scoring a lot worse than
the models trained on hand-annotated datasets, performs best in the
category of synthetic datasets.
Scoring about ten percent points in \shmics{} worse than \shdsgv{}
comes the model trained on the \shdssy{} dataset.
This dataset is missing training samples in the categories \textit{Terrain},
\textit{Truck} and \textit{Train}, that together make up about $\SI{1.88}{\percent}$
of annotated pixels in the \shdscs{} dataset, features less photorealism
than \shdsgv{} and camera poses unlike those found in the annotated datasets.
The worst-performing model is the one trained on \shdsvk{}, a dataset that
lacks annotations for the categories \textit{Sidewalk}, \textit{Wall},
\textit{Fence}, \textit{Person}, \textit{Rider}, \textit{Train}, \textit{Motorcycle}
and \textit{Bicycle}, that make up about $\SI{11.08}{\percent}$ of annotated
pixels in the \shdscs{} dataset. In addition to lacking training classes,
\shdsvk{} also shows other inconsistencies in labeling with other datasets,
like labeling what would otherwise be considered as \textit{Sidewalk} as \textit{Terrain},
symptoms of this are also apparent in \autoref{img:baseline_cs},
where the model trained on on \shdsvk{} estimates the presence of large
amounts of vegetation and terrain in the scene.\footnote{
The effects of a different labeling scheme in \shdsvk{} also
apparent in the corresponding confusion matrices \autoref{tab:apx_cfm_vk_no}
and \autoref{tab:apx_cfm_vk_ki} for categories 6 \& 7 (\textit{Vegetation} and \textit{Terrain})
in \shevgi.}

To mitigate the labeling deficiencies of the \shdsvk{} dataset,
it is usually evaluated using the \shevgi{} subset of segmentation
classes, which includes only classes present in \shdscs{}, as well as in
\shdsvk{}. When compared based on \shmigi{} the models
trained on \shdssy{} and \shdsvk{} perform roughly equal. \\

To also access the domain-transfer capabilities to other datasets
an additional set of evaluations on the same trained models
is performed on the \shdskvv.
The results of this evaluation are presented in \autoref{tab:baseline_ki}.
Additionally a collection of example image for each training setup is shown in
\autoref{img:baseline_ki}.\footnote{See \autoref{img:apx_real_evalki} and
\autoref{img:apx_synthetic_evalki} for a larger collection of example images.}\\

\tableizefile{tables/baseline_ki}
             {tab:baseline_ki}
             {Segmentation quality of network models trained solely on segmentation datasets
               when evaluated on the \shdskvv}
             {Baseline segmentation quality (\shdskvv)}
             {1.0}{ht}

The network models trained on the real, hand-annotated, \shdscs{} dataset show
a significant domain loss in evluation quality, when evaluated on another
dataset, in this case \shdski{}.
The \shmics{} values for the \shrns{} based network structures
are about $20$ percent points lower than those achieved when evaluating
on the \shdscs{} dataset.
For the \shrnl{} based model the \shmics{} is even reduced by nearly
$30$ percent points, further hinting at a possible overfitting on the images
in the source-dataset.
All models trained on synthetic datasets show improved results, when compared
to the scores achieved when evaluating on \shdscs{}.
When compared using \shmigi{}, the model trained on \shdsvk{}
now outperforms the model trained on the \shdssy{} dataset, possibly due to
the \shdsvk{} dataset being designed to mimic images from the real \shdski{} dataset.
The model trained on the \shdsgv{} dataset further outperforms the models
trained on other synthetic datasets by a wide margin.

\figurizefile{diagrams/04_01_baseline_evalki}
             {img:baseline_ki}
             {Comparison of estimates from models trained using different segmentation-only
               training setups when evaluated on the \shdskvv.}
             {Comparision of baseline segmentation quality (\shdskvv)}
             {0.9}{ht}

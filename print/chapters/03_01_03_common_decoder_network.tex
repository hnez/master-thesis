The depth estimation, semantic segmentation and domain discrimination
decoder networks share a common structure, shown in \autoref{img:decoder}.
The decoder network uses the different resolution outputs $\gls{sym:enc_out}_0$-$\gls{sym:enc_out}_4$
from the encoder network as skip connections to generate its outputs
$\gls{sym:dec_out}_0$-$\gls{sym:dec_out}_5$.
Shown in \autoref{table:decoder_outputs} are the decoder input-output
dependencies and the resolutions of the different output images.
Reflection padding is used in the convolutional layers to preserve
image dimensions and $2 \times$ umpsamping with nearest neighbour
interpolation is used to successively reconstruct a high resolution
output image from the lower resolution input images.

\begin{table}[ht]
  \centering
  \begin{tabular}{r | r | r | r | r}
    Name                  & Size increase                   & Example resolution & Number of channels & Inputs used\\ \hline
    $\gls{sym:enc_out}_4$ &           $\gls{sym:enc_out}_4$ &      $20 \times 6$ &              $512$ & - \\ \hline
    $\gls{sym:dec_out}_5$ &           $\gls{sym:enc_out}_4$ &      $20 \times 6$ &              $512$ & $\gls{sym:enc_out}_4$ \\
    $\gls{sym:dec_out}_4$ &  $\gls{sym:enc_out}_4 \times 2$ &     $40 \times 12$ &              $256$ & $\gls{sym:enc_out}_3$ - $\gls{sym:enc_out}_4$ \\
    $\gls{sym:dec_out}_3$ &  $\gls{sym:enc_out}_4 \times 4$ &     $80 \times 24$ &              $128$ & $\gls{sym:enc_out}_2$ - $\gls{sym:enc_out}_4$ \\
    $\gls{sym:dec_out}_2$ &  $\gls{sym:enc_out}_4 \times 8$ &    $160 \times 48$ &               $64$ & $\gls{sym:enc_out}_1$ - $\gls{sym:enc_out}_4$ \\
    $\gls{sym:dec_out}_1$ & $\gls{sym:enc_out}_4 \times 16$ &    $320 \times 96$ &               $32$ & $\gls{sym:enc_out}_0$ - $\gls{sym:enc_out}_4$ \\
    $\gls{sym:dec_out}_0$ & $\gls{sym:enc_out}_4 \times 32$ &   $640 \times 192$ &               $16$ & $\gls{sym:enc_out}_0$ - $\gls{sym:enc_out}_4$
  \end{tabular}

  \caption{Exemplary decoder network input/output dimensions}
  \label{table:decoder_outputs}
\end{table}

\figurizefile{diagrams/03_01_03_decoder_network}
             {img:decoder}
             {Complementary to the encoder network the decoder network
               successively increases the image resolution during processing.}
             {Decoder network structure}
             {0.9}{ht}

To allow for flexible multi-task training setups the decoder network can
be split into a head half and possibly multiple tail halves at various
positions in the network structure.
A diagram illustrating the named split positions \circnum{0} - \circnum{6}
is shown in \autoref{img:decoder_split}. \\

When, for example, split position \circnum{3} is selected to split the decoder
into a common head and one tail for depth estimation and one tail for
semantic segmentation, the last five convolutions are instanciated twice,
once for the depth estimation tail and once for the semantic segmentation tail.
While the first five convolutions are instanciated only once.
Additionaly gradient scaling is set up to be performed in the backpropagation
pass for every connection crossed by the red line denoting split position \circnum{3},
namely on connections $\gls{sym:dec_out}_5$, $\gls{sym:dec_out}_4$,
$\gls{sym:dec_out}_3$, $\gls{sym:enc_out}_1$, $\gls{sym:enc_out}_0$
and between the fifth \acrshort{elu} non-linearity and the third upsampling operation.

\figurizefile{diagrams/03_01_03_decoder_network_split}
             {img:decoder_split}
             {The decoder network is modular, in that splits can be inserted
               at various split positions to perform other operations on the
               intermediate results. The colored lines indicated positions
               of named split positions in the decoder.}
             {Decoder network split positions}
             {0.9}{ht}

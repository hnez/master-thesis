In order to perform backpropagation of the error term
from the loss to the network parameters, each processing step
in between needs to be differentiable.

This includes the coordinate transformation steps \circnum{1}-\circnum{3},
which are linear operations, and as such differentiable,
as well as the sampling performed at step \circnum{4}.

\figurizefile{diagrams/02_02_05_sampling}
             {img:sampling_nn_bili}
             {Comparison of nearest neighbour (left) and bilinear (right) interpolation.
               In case of nearest neighbour interpolation the gradients \acrshort{wrt}
               the sampling position are either $0$ or Dirac-delta shaped, and thus not
               suitable for backpropagation.}
             {Comparison of nearest neighbour and bilinear interpolation}
             {0.9}{ht}

In order to sample color information from the discrete
pixels in the \textit{source} image at the continous
sampling positions produced by step \circnum{3}, interpolation is used.
\autoref{img:sampling_nn_bili} compares two possible
interpolation methods, with a focus on their differentiability
with respect to the sampling position.
Nearest neighbour interpolation to the left and bilinear
interpolation to the right.
In this example both methods are used to up-scale
a $5 \times 5$ grid of pixels of random colors.
Nearest neighbour interpolation uses the color information of the one
pixel closest to the sampling position as output, while bilinear
interpolation uses the four closest pixel's color information.
Bilinear Interpolation is performed according to
\autoref{eq:bilinear_sampling} (adapted from \cite{wiki_xxxx_bilinear}),
where coordinates are normalized to the four pixels around the
sampling point ($(\gls{sym:x}=0, \gls{sym:y}=0)$ is the coordinate of the next pixel to the
top left of the sampling point $(\gls{sym:x}=1, \gls{sym:y}=1)$ is the coordinate of the next pixel
to the bottom right of the sampling point, $\gls{sym:input}_c(1, 1)$ is the intensity
of the $c$ color channel of the input image for said pixel and $\gls{sym:output}_c(0.5, 0.5)$
is the interpolated intensity at the center of the four input pixels).

\begin{align}
  \gls{sym:input_region}_c &=
    \begin{pmatrix}
    \gls{sym:input}_c(0, 0) & \gls{sym:input}_c(0, 1) \\
    \gls{sym:input}_c(1, 0) & \gls{sym:input}_c(1, 1)
  \end{pmatrix} \\
  \gls{sym:output}_c(\gls{sym:x}, \gls{sym:y}) &=
  \begin{pmatrix}
    1 - \gls{sym:x} & \gls{sym:x}
  \end{pmatrix}
  \gls{sym:input_region}_c
  \begin{pmatrix}
    1 - \gls{sym:y} \\
    \gls{sym:y}
  \end{pmatrix}
  \label{eq:bilinear_sampling}
\end{align}

Also shown in \autoref{img:sampling_nn_bili} is a more detailed
view of a single line of the interpolated output and a decoposition
of said line into the respective red/green/blue color components.

While gradients w.r.t. the sampling position for
nearest neighbour interpolation are either zero or the
Dirac-delta function, and thus not suitable for backpropagation,
bilinear interpolation produces usable gradients based on the
color information of the four surrounding input pixels,
as shown by the partial derivates in Equations \ref{eq:dbilidx}
and \ref{eq:dbilidy}.

\begin{align}
  \frac{\partial}{\partial \gls{sym:x}} \gls{sym:output}_c(\gls{sym:x}, \gls{sym:y}) &=
  \begin{pmatrix}
    - 1 & 1
  \end{pmatrix}
  \gls{sym:input_region}_c
  \begin{pmatrix}
    1 - \gls{sym:y} \\
    \gls{sym:y}
  \end{pmatrix}
  \label{eq:dbilidx} \\
  \frac{\partial}{\partial \gls{sym:y}} \gls{sym:output}_c(\gls{sym:x}, \gls{sym:y}) &=
  \begin{pmatrix}
    1 - \gls{sym:x} & \gls{sym:x}
  \end{pmatrix}
  \gls{sym:input_region}_c
  \begin{pmatrix}
    -1 \\
    1
  \end{pmatrix}
  \label{eq:dbilidy}
\end{align}

This kind of differentiable interpolation is also used
in Spatial Transformer Networks \cite{jaderberg_2015_spatial},
where a neural network is used to select patches of an
image to be processed by another neural network. \\

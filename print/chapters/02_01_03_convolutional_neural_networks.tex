Artificial neural networks can be applied to data from
a wide variety of sources of different dimensionality.
Examples for different mayor dimensionalities are:

\begin{description}
  \item[0D] Data from multiple sensors is aggregated
    into a single output measurement.
  \item[1D] Audio data is transformed into a stream
    of phonemes.
  \item[2D] An input image is tranformed into an
    output image.
  \item[3D] An input video sequence is transformed into an
    output video sequence, or volumetric data like scans of
    biologic tissue is analyzed for anomalities.
\end{description}

Such multi-dimensional data may be flattened into a
single vector for use in a classical \gls{fc_nn} architecture.
This flattening of intrinsically multi-dimensional data
does however come with some drawbacks.

If the input vector section contains the pixel color information of an
image with dimensions $\gls{sym:img_height} \times \gls{sym:img_width} = 192 \times 640$
pixels with $\gls{sym:img_channels} = 3$ color channels each,
the corresponding vector has a dimension of $192 \times 640 \times 3 = 368640$ elements,
a weight matrix mapping this flattened input vector to an output
vector of equal size, when using $\SI{32}{bit}$ float datatypes, would
take up roughly $\SI{544}{\giga\byte}$ of storage space,
an impractically large amount for forming a network structure
using this operation as a building block.

By expoiting the the inherent dimesionality of the data,
it is possible to greatly reduce the number of network weights,
while improving learning performance at the same time.
One such approach is a \acrfull{cnn}, which
model the 2D spatial relations of images better than the previously
presented fully connected networks with flattened input vectors.
Common building blocks of \glspl{cnn} are shown in \autoref{img:cnn_layer}.

\figurizefile{diagrams/02_01_03_cnn_layer}
             {img:cnn_layer}
             {\Glspl{cnn} max consist of a variety of components, shown here is a 2D-Convolution,
               an activation function, and a pooling layer}
             {Common \gls{cnn} layer components}
             {0.9}{ht}

\begin{description}
  \item[2D Convolution] The input image of dimension
  $(\gls{sym:img_channels}_1, \gls{sym:img_height}, \gls{sym:img_width})$
  is convolved\footnote{Most deep learning framework actually perform a
  cross-correlation calculation instead of an actual convolution.}
  with $\gls{sym:img_channels}_2$ filter kernels, each of dimension
  $(\gls{sym:img_channels}_1, \gls{sym:kernel_size}, \gls{sym:kernel_size})$, where $\gls{sym:kernel_size}$
  is the kernel size.
  $\gls{sym:img_channels}_2$ kernels of size $3 \times 3$ pixels generate an output image
  of dimension $(\gls{sym:img_channels}_2, \gls{sym:img_height}-2, \gls{sym:img_width}-2)$.
  This covolution operation has
  $(\gls{sym:img_channels}_2, \gls{sym:img_channels}_1, \gls{sym:kernel_size}, \gls{sym:kernel_size})$
  trainable weights, that are optimizeable by backpropagation.

  \item[Non-Linear Activation] As was the case for fully-connected
  networks, each intermediate element is fed into a non-linear activation
  function to enable learning of non-linear input/output mappings.

  \item[2D Max Pooling] The kernel sizes of typical \glspl{cnn} are usually
  quite small, when compared to the input image size.
  This means that only spatially close features of the input image
  are combined to form the output image.
  By reducing the image size between convolution layers, the same
  kernel sizes can occupy larger parts of the input image.
  A common approach to resolution reduction is to only use
  the largest output value from a block of e.g. $2 \times 2$ pixels
  to form the output images, reducing the image size by a factor of $2$
  in each image dimension.
\end{description}

\noindent
An equation for the convolution operation, as implemented in most deep
learning frameworks, is shown in \autoref{eq:2d_conv}, where
$\gls{sym:img_channels}_{\sigma}$ is the set of input channels,
$\gls{sym:img_channels}_{\tau}$ is the set of output channels,
$c_{\tau} \in \gls{sym:img_channels}_{\tau}$,
$\gls{sym:kernel_size} + 1$ is the kernel size (e.g. $3$),
\gls{sym:kernel} is the convolution kernel,
$\gls{sym:sc_kernel}_{\sigma,\tau,u,v}$ are the learnable kernel weights,
$\gls{sym:input}_{\sigma}$ is a 2D array of input color values
for the color channel $\sigma$ and $\gls{sym:output}_{\tau}$ is a 2D array of
output features for channel $\tau$.
Deep learning frameworks also allow the usage of a striding parameter that
results in only every nth row and column of the convolution output being used.
A striding of $1$ results in all rows and columns of the output being used,
a stride of $2$ results in only every second row and column being used,
and thus a $/2$ reduction in both image dimensions and a $/4$ reduction in
output pixels.

\begin{equation}
  \gls{sym:output}_{\tau}(x, y) =
  \sum_{\sigma \in \gls{sym:img_channels}_{\sigma}}
  \sum_{u=0}^{\gls{sym:kernel_size}}
  \sum_{v=0}^{\gls{sym:kernel_size}}
  \gls{sym:input}_c(x + u,  y + v)
  \; \gls{sym:kernel}_{\sigma,\tau,u,v}
  \label{eq:2d_conv}
\end{equation}

\noindent
A common non-linear activation function for image recognition tasks
is the \gls{relu} function, shown in \autoref{eq:relu}.

\vspace{0.5cm}
\begin{minipage}[H]{.48\textwidth}
  \begin{equation}
    \acrshort{relu}(\gls{sym:sc_input}) =
    \begin{cases}
      0 & \gls{sym:sc_input} < 0 \\
      \gls{sym:sc_input} & \gls{sym:sc_input} \geq 0
    \end{cases}
    \label{eq:relu}
  \end{equation}
\end{minipage}
\hfill\vline\hfill
\begin{minipage}[H]{.48\textwidth}
  \begin{equation}
    \frac{\partial}{\partial \gls{sym:sc_input}} \acrshort{relu}(\gls{sym:sc_input}) =
    \begin{cases}
      0 & \gls{sym:sc_input} < 0 \\
      1 & \gls{sym:sc_input} \geq 0
    \end{cases}
    \label{eq:drelu_dx}
  \end{equation}
\end{minipage}
\vspace{0.5cm}

\noindent
An application for \glspl{cnn} is the classification of images,
as performed in the ImageNet challenge \cite{russakovsky_2015_imagenet_challenge}.
An example of a participant in this chellenge are the ResNet class of
networks \cite{he_2016_resnet}, which combine convolutional
input layers with a conventional fully connected output layer
to assign an input image to one of $1000$ classes and
which, without its fully-connected output layers, is used as a
backbone network in this thesis.

To ease development of complex neural network structures PyTorch \cite{paszke_2017_pytorch_autograd},
the deep learning framework used in this thesis, automatically performes
the gradient calculations needed for the backpropagation.
PyTorch uses reverse-mode automatic differentiation.
This section describes the general concept of reverse-mode automatic
differentiation by example. \\

The example uses a simple output layer with inputs $\gls{sym:sc_input}_a$, $\gls{sym:sc_input}_b$ and $\gls{sym:sc_input}_c$
and corresponding weights $\gls{sym:weight}_a$, $\gls{sym:weight}_b$ and $\gls{sym:weight}_c$, that are to
be optimized to output a single scalar value $\gls{sym:sc_output}_y$.
The optimization objective is to minimize the square error between
the networks output and $\gls{sym:sc_output}_y$.
The corresponding equation to calculate the layer's output and
loss \gls{sym:loss} is shown in \autoref{eq:fcnn-mini-ex}.

\begin{equation}
  \gls{sym:loss} = \left(
  \begin{pmatrix}
    \gls{sym:sc_input}_a & \gls{sym:sc_input}_b & \gls{sym:sc_input}_c
  \end{pmatrix}
  \begin{pmatrix}
    \gls{sym:weight}_a \\
    \gls{sym:weight}_b \\
    \gls{sym:weight}_c
  \end{pmatrix}
  - \gls{sym:sc_output}_y
  \right)^2
  \label{eq:fcnn-mini-ex}
\end{equation}

\autoref{eq:step_step_loss} shows exemplary values for the layer's
inputs $\gls{sym:sc_input}_a$ to $\gls{sym:sc_input}_c$ and weights
$\gls{sym:weight}_a$ to $\gls{sym:weight}_c$, as well as a desired output $\gls{sym:sc_output}_y$.
To simplify the algorithmic calculation of the gradients in
the backpropagation step the calculations are decomposed into
to the basic operations $\times$ (multiplication) and $+$ (addition).
From these decomposed calculations and intermediate results
a dependency graph is generated, as shown in \autoref{img:autograd}.

\begin{figure}[H]
  \begin{minipage}[t]{.48\textwidth}
    \begin{align*}
      \gls{sym:sc_input}_a &= 2 \\
      \gls{sym:sc_input}_b &= 2 \\
      \gls{sym:sc_input}_c &= -1 \\
      \gls{sym:sc_output}_y &= 1 \\
      \gls{sym:weight}_a &= 1 \\
      \gls{sym:weight}_b &= 2 \\
      \gls{sym:weight}_c &= 4 \\
    \end{align*}
  \end{minipage}
  \hfill\vline\hfill
  \begin{minipage}[t]{.48\textwidth}
    \begin{align*}
      \gls{sym:sc_output}_y' &= -1 \times \gls{sym:sc_output}_y = -1 \\
      \gls{sym:sc_input}_a' &= \gls{sym:sc_input}_a \times \gls{sym:weight}_a = 2 \\
      \gls{sym:sc_input}_b' &= \gls{sym:sc_input}_b \times \gls{sym:weight}_b = 4 \\
      \gls{sym:sc_input}_c' &= \gls{sym:sc_input}_c \times \gls{sym:weight}_c = -4 \\
      v &= \gls{sym:sc_input}_a' + \gls{sym:sc_input}_b' + \gls{sym:sc_input}_c' = 2 \\
      d &= \gls{sym:sc_output}' + o = 1 \\
      \gls{sym:loss} &= d \times d = 1
    \end{align*}
  \end{minipage}

  \caption[Step-by-step loss calculation]{An exemplary order of computations that fulfills the
  dependency requirements of intermediate values and results in the loss value}
  \label{eq:step_step_loss}
\end{figure}

In PyTorch a dependency graph, like the one shown in \autoref{img:autograd},
is automatically generated when basic operations like multiplications
or additions are performed on PyTorch variables.

\figurizefile{diagrams/02_01_02_autograd}
             {img:autograd}
             {A dependency graph of the loss \gls{sym:loss}. An intermediate value depends on all values pointing towards it}
             {Loss dependency graph}
             {0.9}{ht}

Using the dependency graph it is possible to numerically calculate the
required gradients $\frac{\partial \gls{sym:loss}}{\partial \gls{sym:weight}_a}$,
$\frac{\partial \gls{sym:loss}}{\partial \gls{sym:weight}_b}$
and $\frac{\partial \gls{sym:loss}}{\partial \gls{sym:weight}_c}$.
Without requiring a more powerfull symbolic differentiation engine,
meaning one that is capable of solving general derivate functions.
Gradients are unraveled using the chain rule and the differentiation rules
of the basic operations back-to-front, starting at the loss output $l$.
In the automatic differentiation engine, known intermediate values for gradients
and known variables are eagerly replaced by the numeric values, removing the need
for symbolic evaluations.
The calculations for the given values are shown in \autoref{eq:fcnn_backprop}.

\begin{figure}[H]
  \begin{align*}
    \dlossd{d} &= d + d = 1 + 1 = 2\\
    \dlossd{o} &= \frac{\partial d}{\partial o} \dlossd{d} = 1 \times 2 = 2 \\
  \end{align*}
  \begin{minipage}[H]{.48\textwidth}
    \begin{align*}
      \dlossd{a'} &= \frac{\partial o}{\partial a'} \dlossd{o} = 1 \times 2 = 2 \\
      \dlossd{b'} &= \frac{\partial o}{\partial b'} \dlossd{o} = 1 \times 2 = 2 \\
      \dlossd{c'} &= \frac{\partial o}{\partial c'} \dlossd{o} = 1 \times 2 = 2
    \end{align*}
  \end{minipage}
  \hfill\vline\hfill
  \begin{minipage}[H]{.48\textwidth}
    \begin{align*}
      \dlossd{\gls{sym:weight}_a} &= \frac{\partial a'}{\partial \gls{sym:weight}_a} \dlossd{a'} = a \times 2 = 4 \\
      \dlossd{\gls{sym:weight}_b} &= \frac{\partial b'}{\partial \gls{sym:weight}_b} \dlossd{b'} = b \times 2 = 4 \\
      \dlossd{\gls{sym:weight}_c} &= \frac{\partial c'}{\partial \gls{sym:weight}_c} \dlossd{c'} = c \times 2 = -2
    \end{align*}
  \end{minipage}

  \caption[Calculation of intermediate gradients]{The dependency graph from \autoref{eq:fcnn_backprop}
  is traversed in reverse direction to calculate gradients of intermediate values}
  \label{eq:fcnn_backprop}
\end{figure}

The layer's weights are then updated for the next iteration
by using stochastic gradient descent. Gradient descent adapts the weights
by a small learning rate \gls{sym:learning_rate} in the direction of lowering
gradient $\gls{sym:vec_weight}'= \gls{sym:vec_weight} - \gls{sym:learning_rate}\dlossd{\gls{sym:vec_weight}}$.
An example of gradient descent with a learning
rate of $\gls{sym:learning_rate} = 0.01$ is shown in \autoref{eq:fcnn_update},
where $\gls{sym:weight}_a'$, $\gls{sym:weight}_b'$ and $\gls{sym:weight}_c'$
are the weights to be used in the next iteration.

\begin{figure}[H]
  \begin{align*}
    \gls{sym:learning_rate} &= 0.01 \\
    \gls{sym:weight}_a' &= \gls{sym:weight}_a - \gls{sym:learning_rate} \dlossd{\gls{sym:weight}_a} = 1 - 0.04 = 0.96 \\
    \gls{sym:weight}_b' &= \gls{sym:weight}_b - \gls{sym:learning_rate} \dlossd{\gls{sym:weight}_b} = 1 - 0.04 = 0.96 \\
    \gls{sym:weight}_c' &= \gls{sym:weight}_c - \gls{sym:learning_rate} \dlossd{\gls{sym:weight}_c} = 1 + 0.02 = 1.02
  \end{align*}

  \caption[Weight adaption by gradient descent]{The the network's weights are adapted
  using the product of the learning rate \gls{sym:learning_rate} and their respective gradient}
  \label{eq:fcnn_update}
\end{figure}

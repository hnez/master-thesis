The following sections discuss the training of said
artificial neural networks for depth estimation from
monocular images.
Two approaches are discussed, one being a supervised
approach that uses image data and corresponding
depth information and a self-supvervised approach
based only on image data. \\

Training data for supervised depth training comes from
one of two main sources, either synthetic data generated
by simulation or real-world data from specialized depth sensors.

The first approach uses computer graphics rendering software,
which in addition to color information
is also able to generate per-pixel depth information.
The other source of depth training data are camera rigs that
are additionally equipped with distance-measuring devices
like \acrshort{lidar}-Scanners.
Both methods have several benefits and drawbacks, as
shown in Figures \ref{img:synthetic_depth}
and \ref{img:kitti_lidar_depth} and accompanying lists.

\noindent
\begin{minipage}[t]{.5\textwidth}
  \figurizegraphic{images/02_02_03_synthia_synthetic.png}
                  {img:synthetic_depth}
                  {Depth imagery generated using computer graphics \cite{ros_2016_synthia}}
                  {A depth image from a synthetic source}
                  {0.9}{H}
  \begin{description}
    \item[\texttt{+}] Dense (per-pixel) depth information.
    \item[\texttt{-}] Images have a dissimilar look in comparison
    to to real camera images.
    \item[\texttt{-}] Time intensive model design.
    \item[\texttt{+}] Easy generation of large amounts of images from different camera poses.
  \end{description}
\end{minipage}
\begin{minipage}[t]{.5\textwidth}
  \figurizegraphic{images/02_02_03_kitti_lidar.png}
                  {img:kitti_lidar_depth}
                  {Depth imagery generated using a LIDAR sensor \cite{geiger_2013_kitti}}
                  {A depth image from a sensor-based source}
                  {0.9}{H}

  \begin{description}
    \item[\texttt{-}] Sparse depth information, as the number of
    rays sent out by the LIDAR is small, when compared
    to the number of image pixels.
    Also for angled, reflective surfaces and large
    distances reflected rays may not be registered.
    \item[\texttt{+}] High quality color images.
    \item[\texttt{-}] High hardware costs for the LIDAR device.
  \end{description}
\end{minipage}\\

\vspace{0.5cm}

While the choice of datasets for a monocular supervised
depth estimation training is somewhat restricted by the
need of depth ground truth, the training setup is relatively
straight forward, as shown in \autoref{img:supervised_depth}.

A neural network capable of image-to-image transformation,
like a fully convolutional network, is initialzed with
arbitrary weights, and used to predict a depth map for
an input image.
The error between this estimate and the given depth ground
truth is gauged by a loss function and the network
is optimized to reduce this error for every image/ground truth
pair in the dataset.

\figurizefile{diagrams/02_02_03_supervised_depth}
             {img:supervised_depth}
             {In a supervised learning setup the neural network is supplied with
               exemplary input data and corresponding expected output data.
               In case of depth estimates these are either images from real-world
               sensors or ones generated using computer graphics}
             {A supervised depth training setup}
             {0.9}{ht}

While the overall training setup matches that of a
supervised depth estimation training, the calulation of
the loss requires special attention.

A color image consists of three color components per
pixel, a color image thus has a horizontal and vertical
dimension and three channels.
The depth estimation task produces one depth value per
output pixel, accordingly it has a single output channel.
A segmentation estimate consists of a per-pixel probability
estimate for each class, an example for a single image pixel
is shown in \autoref{img:semseg_prob}, where said pixel is
estimated to depict either a person or a person riding a
vehicle with high probabilities.
In a training environment with, for example, a selection of 20 segmentation classes,
the segmentation estimate has 20 output channels.
The ground truth segmentation data, on the other hand,
is one discrete class number per pixel, meaning a single channel.

\figurizefile{diagrams/02_03_01_propabilities}
             {img:semseg_prob}
             {Exemplary output of a semantic segmentation network for a single pixel}
             {Output of a semantic segmentation network for a single pixel}
             {0.9}{ht}

To be a probability measure the segmentation estimates
have to satisfy the requirements, that the individual class
probabilities have to lie in the interval
$\gls{sym:prob_dist}(\gls{sym:prob_container}) \in [0, 1]$
and all class probabilities have to sum to one
$\sum\nolimits{}_{\gls{sym:prob}_{\gls{sym:class_index}} \in \gls{sym:prob_container}}\gls{sym:prob_dist}(\gls{sym:prob}_{\gls{sym:class_index}}) = 1$.

One method that fulfills these requirements for every
set of input values is the usage of the $SoftMax$ activation
function as output layer of a neural network.
The definition of $SoftMax$ is shown in the following equation,
where $\gls{sym:prob}_{\gls{sym:class_index}}$ is the estimated probability of class $\gls{sym:class_index}$,
$\gls{sym:output}_{\gls{sym:class_index}}$ is the raw output value of the network for class $\gls{sym:class_index}$
and $\gls{sym:prob_container}$ is the set of all output classes.

\begin{equation}
  \gls{sym:prob}_{\gls{sym:class_index}}
  = SoftMax(\gls{sym:output}_{\gls{sym:class_index}})
  = \frac{
      \exp{
        \gls{sym:output}_{\gls{sym:class_index}}
      }
    }
    {
      \sum_{i \in \gls{sym:prob_container}}
      \exp{\gls{sym:output}_i}
    }
\end{equation}

A complementary loss function that is commonly used in
combination with the $SoftMax$ activation function, is
the \acrfull{cel}.

\begin{align}
  \gls{sym:loss} &= CrossEntropy(
    \gls{sym:output}_{\gls{sym:class_index}}
  ) \\
  &= -\log \left( SoftMax(
    \gls{sym:output}_{\gls{sym:class_index}}
  ) \right) \\
  &= -\gls{sym:output}_{\gls{sym:class_index}}
  + \log \left(
    \sum_{i \in \gls{sym:prob_container}}
    \exp{\gls{sym:output}_i}
  \right)
\end{align}

Where $\gls{sym:class_index}$ is the correct class, as given by the ground
truth data, $\gls{sym:output}_{\gls{sym:class_index}}$ is the corresponding raw network output
value and $\gls{sym:loss}$ is the loss value.

The optimization criterion is the maximization of the
estimated class log-likelihoods, or the minimization of the
negative class log-likelihood, of the correct class.

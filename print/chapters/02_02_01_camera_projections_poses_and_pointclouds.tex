The easiest camera model is that of a pinhole camera,
where light from the scene passes through a tiny aperture
and is projected onto an image plane.
An example of such a projection is shown
in \autoref{img:pinhole_camera_basics}.

Points in the scene are projected based on the intersection
of a line, defined by the camera aperture, the point in the
scene, with the image plane.
The resulting image is flipped in the horizontal and vertical
dimension.
This is demonstrated in the left third of \autoref{img:pinhole_camera_basics},
where the leftmost edge of the red rectangle is projected to the
rightmost edge of the image plane.
All projection lines converge at the aperture, in the middle of the
diagram, and diverge again to form the projected image. \\

Another camera model, that does not exhibit the image-flipping behaviour,
places the virtual image plane in front of the camera aperture,
like a canvas or window.
The projection of points is defined by the same line intersection
as before, just with the image plane in a different position along the
projection line.
This model is used throughout this thesis and is also shown
in the cental diagram in \autoref{img:pinhole_camera_basics}.

\figurizefile{diagrams/02_02_01_point_camera}
             {img:pinhole_camera_basics}
             {A camera model describes the mapping of points in 3D-space
               to an image plane. In a pinhole camera model this mapping
               is defined by the intersection of a line from the camera's
               aperture to the point ins space and an image plane with
               distance \gls{sym:focal_length}}
             {Pinhole camera model}
             {0.9}{ht}

The distance between the pinhole camera's aperture and the
image plane is called the focal length \gls{sym:focal_length} and it affects
the visible area of the image, the angle of view \gls{sym:angle_of_view}.
Going from a smaller focal length $\gls{sym:focal_length}_2$ to a larger
focal length $\gls{sym:focal_length}_1$ decreases the angle of view from a
larger angle $\gls{sym:angle_of_view}_2$ to a smaller angle $\gls{sym:angle_of_view}_1$. \\

Given a projection plane of size \gls{sym:img_width}, and a desired angle of
view of $\gls{sym:angle_of_view}$, a point in $(\gls{sym:x}, \gls{sym:y}, \gls{sym:z})^T$ in 3D-space
can be projected into 2D-homogeneous coordinates by using the projection matrix \gls{sym:proj_matrix}
presented in \autoref{eq:three_d_homo_proj} and from there into 2D-coordinates
$(\gls{sym:x}_p, \gls{sym:y}_p)^T$ on the image plane by using \autoref{eq:two_d_proj}.

\vspace{0.5cm}
\begin{minipage}[H]{.48\textwidth}
  \begin{align}
    \gls{sym:focal_length} &= \frac{\gls{sym:img_width}}{2}
    \mathrm{cot} \left( \frac{\gls{sym:angle_of_view}}{2} \right) \\
    \gls{sym:proj_matrix} & =
    \begin{pmatrix}
      \gls{sym:focal_length} & 0                      & 0 \\
      0                      & \gls{sym:focal_length} & 0 \\
      0                      & 0                      & 1 \\
    \end{pmatrix}
  \end{align}
\end{minipage}
\hfill\vline\hfill
\begin{minipage}[H]{.48\textwidth}
  \begin{align}
    \begin{pmatrix}
      \gls{sym:x}_p^h \\ \gls{sym:y}_p^h \\ \gls{sym:homo_w}
    \end{pmatrix}
    &= \gls{sym:proj_matrix}
    \begin{pmatrix}
      \gls{sym:x} \\ \gls{sym:y} \\ \gls{sym:z}
    \end{pmatrix}
    \label{eq:three_d_homo_proj} \\
    \begin{pmatrix}
      \gls{sym:x}_p \\ \gls{sym:y}_p
    \end{pmatrix}
    &=
    \begin{pmatrix}
      \gls{sym:x}_p^h / \gls{sym:homo_w} \\ \gls{sym:x}_p^h / \gls{sym:homo_w}
    \end{pmatrix}
    \label{eq:two_d_proj}
  \end{align}
\end{minipage}
\vspace{0.5cm}

While two images of the same scene, taken at the same point in
time, but different points in space, as shown in \autoref{img:camera_poses}
look different on the outside, they are different projections
of the same underlying data.
The data in this case being the color of points in 3D-space.

\figurizefile{diagrams/02_02_01_poses}
             {img:camera_poses}
             {A change of pose results in a difference in the projected images,
               even though the underlying 3D-data is the same.}
             {A scene, as seen from two different camera poses}
             {0.9}{ht}

The viewpoint of a camera is influenced by its position and
its pointing direction or heading. The combination of
camera position and heading is called the camera pose.
To perform a coordinate transformation between one
camera pose and anothere, six pieces of information are required.
Three values describing the translation between the poses
and three describing the relative rotation. \\

The projection of a 3D scene onto an image plane is a lossy
conversion, meaning that the scene is not fully reconstructible
from the image data. Information losses occur due to the
following processes:

\begin{samepage}
  \begin{description}
    \item[Occlusion] Color information is only sampled from points
    that are visible to the camera and not occluded by other objects.

    \item[Spatial Quantisation] The image information is quantized
    into a finite amount of discrete pixels.

    \item[Distance Ambiguity] The color of a pixel depends on the
    color of the first object along a line, spanned by the pixel on
    the camera sensor and the cameras aperture, but not on the
    distance to said object.
    Every point on said line is projected onto the same position
    in the image plane.
  \end{description}
\end{samepage}

\noindent
\autoref{img:pixels} shows the effect of quantizing
the projected scene into discrete pixels and shows
the ambiguity of these projected pixels with respect
to distance from the camera, by drawing a line from
the camera aperture through the projected pixel position
on the image plane into 3D space.
The equation, based on the inverse camera projection matrix
$\gls{sym:proj_matrix}^{-1}$, for the line $\gls{sym:point}(\gls{sym:homo_w})$ in 3D-space corresponding
to the projected pixel position $(\gls{sym:x}_p, \gls{sym:y}_p)^T$ is given
in \autoref{eq:backproj}. \\

\begin{equation}
  \gls{sym:point}(\gls{sym:homo_w}) =
  \begin{pmatrix}
    \gls{sym:x} \\ \gls{sym:y} \\ \gls{sym:z}
  \end{pmatrix}
  = \gls{sym:proj_matrix}^{-1}
  \begin{pmatrix}
    \gls{sym:x}_p \\ \gls{sym:y}_p \\ \gls{sym:homo_w}
  \end{pmatrix}
  \label{eq:backproj}
\end{equation}

\figurizefile{diagrams/02_02_01_pixels}
             {img:pixels}
             {The projection from 3D-space to discrete 2D-pixels is a lossy
               process. Information loss occurs due to the projection and the
               quantization into pixels}
             {Information loss due to projection}
             {0.9}{ht}

The effect of distance ambiguity is reversible by
adding back in prior knowledge of the distance \gls{sym:homo_w}
of the depicted points from the camera, e.g. by
adding depth information.
This Process is shown in \autoref{img:pointcloud},
where the map of pixel colors is combined with
a map of depth/distance informations to reconstruct
a sparse countour of the scene.
While there is a discrete amount of reconstructed
points, they are placed in the scene in a
continous manner.
Such a mixed representation of a scene as a discrete
number of points distributed in continous space is
referred to as a pointcloud.

\figurizefile{diagrams/02_02_01_pointcloud}
             {img:pointcloud}
             {By supplying external knowledge about the distance of a pixel,
               the 3D-scene can be partially reconstructed.
               The distance information is presented as a color-coded map,
               where lighter colors indicate an object closer to the camera.
               The result is a pointcloud of discrete points positioned in
               continuous 3D-space.}
             {Reconstruction of a pointcloud using external depth information}
             {0.9}{ht}

Sources of such prior knowledge of per-pixel depth
are e.g. LIDAR sensors \cite{gaidon_2016_virtual_kitti} or
very fast gated cameras \cite{gruber_2019_gated}.

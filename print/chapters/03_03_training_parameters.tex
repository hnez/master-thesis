The Neural Network models used in this thesis are trained using the
PyTorch Deep Learning Framework \cite{paszke_2019_pytorch}.
The Parameters are optimized using the Adam optimizer \cite{kingma_2015_adam}
with an initial learning rate of $\SI{1e-4}{}$ that is reduced to $\SI{1e-5}{}$
after $3/4$ of the training iterations. Weight decay is not used. \\

Training is performed using two kinds of accelerator hardware,
as listed in \autoref{table:accelerator_hardware}.

\begin{table}[ht]
  \centering
  \begin{tabular}{c | c | c}
    Name   & Description          & GPU RAM \\ \hline
    P100   & Tesla P100 SXM2 16GB & $\SI{16}{\giga\byte}$ \\
    1080Ti & GeForce GTX 1080 Ti  & $\SI{11}{\giga\byte}$
  \end{tabular}

  \caption{Available training-acceleration hardware}
  \label{table:accelerator_hardware}
\end{table}

To simplify the comparison of networks trained on different datasets,
the length of a training iteration, or epoch, is always based on the number
of images in the \shdski{} dataset, even in trainings where this dataset is not used.
A training sample for self-supervised depth training consists of three
consecutive video frames. As shown in \autoref{table:kitti_depth_test_data},
the \shdski{} dataset contains $57874$ such samples. Using this amount
of samples and the depth estimation batch size, a number of batches per
epoch is calculated that is used for every training setup, regardless
of the actual number of depth estimation samples available in the dataset
at hand.
The default learning setup uses a batch size of $\gls{sym:batch_size}_{D} = 12$
depth samples and $\gls{sym:batch_size}_{S} = 6$ segmentation images,
that are processed in one go, resulting in an epoch consisting of
$57874 / \gls{sym:batch_size}_{D} = 4822$ batches.
Trainings are performed at two image resolutions, resolution H, for high,
and resolution L, for low, and using two encoder network structures,
a \shrns{} based encoder (18) and a \shrnl{} based encoder (50).
For trainings at higher resolutions and with larger encoder network network models the maximum
batch size is limited by the amount of available \acrshort{gpu} \acrshort{ram}.
A summary of the batch sizes used and resulting batches per epoch is shown
in \autoref{tab:train_setups}.

\tableizefile{tables/train_setups}
             {tab:train_setups}
             {An overview of the different batch sizes used in trainings.
               The maximum batch sizes are limited by memory constraints of the
               accelerator hardware.}
             {Overview of training batch sizes}
             {1.0}{ht}

Also shown in \autoref{tab:train_setups} are the two special dataset
setups \shdskis{} and \shdscss{}. The first being a variant of the
\shdski{} dataset, where the next reference frame is replaced by the
corresponding current frame from the other stereo camera.
The second being a variant of the \shdscs{} dataset where instead of
the previous and next frame being used as reference frames, the
next frame after/before that is used instead. This reduces the effective
framerate of the \shdscs{} dataset from $\SI{17}{\hertz}$ to
$\SI{8.5}{\hertz}$, matching more closely the $\SI{10}{\hertz}$ of the
\shdski{} dataset. \\

In order to perform compound training on datasets from different sources
their image resolutions have to be adapted to common values.
This includes first resizing to mostly match one image dimension and then
cropping to match the resolutions completely. The resizing dimensions
are choosen per-dataset, based on their native resolutions and
aspect ratios. The resolutions used are shown in \autoref{tab:train_setups_sizes}.
For semantic segmentation training samples a random area is cropped
from the resized images. For depth estimation training samples the
images are horizontally cropped at the center and vertically cropped
to align the horizons.

\tableizefile{tables/train_setups_sizes}
             {tab:train_setups_sizes}
             {An overview of the image resolutions used in training.}
             {Overview of training image sizes}
             {1.0}{ht}

To weight the effect of different output networks on the common parts
of the network, gradient scaling is used. The respective gradient scaling
factors $\gls{sym:gradient_scaling}_\text{depth}$ and
$\gls{sym:gradient_scaling}_\text{segmentation}$ vary between 0 and 1
and are specified per-experiment in the evaluation section.
When a domain discriminator network is used the absolute value of
the respective gradient reversal factor \gls{sym:gradient_reversal}
also specified per-experiment. The domain discriminator has
shown to cause instabilities in training, this effect is mitigated
by always setting the gradient reversal factor \gls{sym:gradient_reversal}
to zero in the first epoch and updating it to the desired value
in later iterations.

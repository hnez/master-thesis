A neural network is a highly parameterized mathematical
model $\gls{sym:fun}_{\gls{sym:weight}}(\gls{sym:input}) \rightarrow \gls{sym:output}$
that is designed to be, given the correct
parameters \gls{sym:weight}, able to perform a specific task.
This task is usually mapping some input data \gls{sym:input},
via a complex, non-linear mapping, to some output data \gls{sym:output}. \\

Neural network models exist in two phases. The
training phase is used to adapt the network parameters
\gls{sym:weight} to perform a specified task.
This phase makes use of exemplary input and output
data to update the parameters to match this data.
The other phase is the evaluation phase, where the
network parameters stay constant and the network model
is used on previously unseen data.

Shown in \autoref{img:nn_basics} are some common elements
in a neural network training setup.
The black dots correspond to scalar values.
The input \gls{sym:input} of the network, as well as intermediate results,
are made up of multiple scalar values, as such they are
expressed as vectors.
The blue lines depict a connection between input values
and output values of a layer, each of these connections
has a weight \gls{sym:weight} associated to it.
While these weights may be learned to be $0$, in theory
there is a connection between each input value of a layer
and each output of said layer in a network, giving this network
structure its name \acrfull{fc_nn}.
The calculation of a layers output values as a weighted
combination of its input values is expressible as
an equivalent vector-matrix multiplication.

To enable more complex input/output-mappings
it is common to stack multiple such layers,
forming a deep network structure.
The subsequent multiplications of an input vector
with different weight matrices is however a linear
process and can as such be reduced to a single
vector-matrix multiplication.
In order to benefit from multi-layer network structures
so-called activation functions are used in between
network layers that have a non-linear input/output mapping
and are independently applied to each output scalar of a layer.
In \autoref{img:nn_basics} the output of an activation function
is depicted as red dots.

\figurizefile{diagrams/02_01_01_nn_basics}
             {img:nn_basics}
             {The forward-pass of a \acrshort{fc_nn} is a chain of vector-matrix multiplications and non-linearities.
             For each weight a gradient \acrshort{wrt} the loss is calculated}
             {General concept drawing of a neural network training}
             {0.9}{ht}

Artificial Neural Networks are trained by supplying exemplary
input data \gls{sym:input} and corresponding expected output
data $\gls{sym:output}_\text{GT}$, and minimizing
the difference \gls{sym:loss} between generated output \gls{sym:output}
and expected output for the available samples.
The difference, or loss \gls{sym:loss} is calculated based on the estimated and
expected output using a differentiable loss function
$\gls{sym:no_fun}(\gls{sym:output}_\text{GT}, \gls{sym:output}) \rightarrow \gls{sym:loss}$.
The quality of a trained neural network model is gauged
based on the quality of estimates on previously unseen
input data.

The training of an artificial neural network is an
interative process, where the initial network weights
are set to random values, and the stop condition is
set based on some expected output quality or a fixed
number of iterations.
During raining the following operations are
performed in a loop:

\begin{description}
  \item[Forward Propagation] ($\gls{sym:fun}(\gls{sym:input}) \rightarrow \gls{sym:output}$)
  Input data is fed into the network and an output is produced.

  \item[Loss Calculation] ($\gls{sym:no_fun}(\gls{sym:output}_{GT}, \gls{sym:output}) \rightarrow \gls{sym:loss}$)
  The error between the produced output and the expected output is calculated
  by some means. This error is usually called loss
  and is to be minimized.

  \item[Backpropagation] ($\dlossd{\gls{sym:weight}}$)
  As hinted at in the bottom right of
  \autoref{img:nn_basics} the loss value \gls{sym:loss}
  is a function of every weight in the network.
  While, due to its high dimensionality, the true shape
  of this function w.r.t. to every weight is not known,
  it is possible to estimate the gradient of this function,
  using differentiation, by backpropagating the error from
  the output towards the inputs of the network.

  \item[Weight Adaption] ($\gls{sym:weight}'= \gls{sym:weight} - \gls{sym:learning_rate}\dlossd{\gls{sym:weight}}$)
  The weights of the network are updated, by a small amount, the learning rate \gls{sym:learning_rate},
  in the direction of lowering gradient. This reduces the loss for the given input/output combination.
\end{description}

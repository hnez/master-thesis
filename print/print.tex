\input{ballast}
\input{shorthands}
\input{colors_tango}
\input{colors_cityscapes}
\input{glossary}
\input{symbols}
\addbibresource{references.bib}

\begin{document}
  \begin{fluff}{-1}{1}
    \includepdf[pages=-]{chapters/00_01_title_page}
    \includepdf[pages=-]{chapters/00_02_eidesstatt}

    \begin{topchapter*}{Abstract}
      \input{chapters/00_03_abstract}
    \end{topchapter*}

    \newpage
    \tableofcontents

    \newpage
    \addcontentsline{toc}{chapter}{List of Tables}
    \listoftables

    \newpage
    \addcontentsline{toc}{chapter}{List of Figures}
    \listoffigures

    \newpage
    \printglossary[type=\acronymtype,title={List of Abbreviations}]

    \newpage
    \printglossary[type=symbolslist,title={List of Symbols}]
  \end{fluff}

  \begin{topchapter}{Introduction}
    \input{chapters/01_introduction}
  \end{topchapter}

  \begin{topchapter}{Theoretical Background}
    \input{chapters/02_theoretical_background}

    \begin{subchapter}{Neural Networks}
      \input{chapters/02_01_neural_networks}

      \begin{subsubchapter}{Fully-Connected Neural Networks}
        \input{chapters/02_01_01_fully_connected_neural_networks}
      \end{subsubchapter}

      \begin{subsubchapter}{Automatic Differentiation}
        \input{chapters/02_01_02_automatic_differentiation}
      \end{subsubchapter}

      \begin{subsubchapter}{Convolutional Neural Networks}
        \input{chapters/02_01_03_convolutional_neural_networks}
      \end{subsubchapter}

      \begin{subsubchapter}{Fully-Convolutional Neural Networks}
       \input{chapters/02_01_04_fully_convolutional_neural_networks}
      \end{subsubchapter}
    \end{subchapter}

    \begin{subchapter}{Depth Estimation}
      \input{chapters/02_02_depth_estimation}

      \begin{subsubchapter}{Camera Projections, Poses and Pointclouds}
        \input{chapters/02_02_01_camera_projections_poses_and_pointclouds}
      \end{subsubchapter}

      \begin{subsubchapter}{Stereo/Monocular Depth Estimation}
        \input{chapters/02_02_02_stereo_monocular_depth_estimation}
      \end{subsubchapter}

      \begin{subsubchapter}{Supervised Depth Estimation Training}
        \input{chapters/02_02_03_supervised_depth_estimation_training}
      \end{subsubchapter}

      \begin{subsubchapter}{Self-Supervised Depth Estimation Training}
        \input{chapters/02_02_04_self_supervised_depth_estimation_training}
      \end{subsubchapter}

      \begin{subsubchapter}{Differentiable Sampling}
        \input{chapters/02_02_05_differentiable_sampling}
      \end{subsubchapter}

      \newpage

      \begin{subsubchapter}{Minimum Reprojection Loss}
        \input{chapters/02_02_06_minimum_reprojection_loss}
      \end{subsubchapter}
    \end{subchapter}

    \begin{subchapter}{Semantic Segmentation}
      \input{chapters/02_03_semantic_segmentation}

      \begin{subsubchapter}{Cross Entropy Loss}
        \input{chapters/02_03_01_cross_entropy_loss}
      \end{subsubchapter}

      \begin{subsubchapter}{Mean Intersection over Union}
        \input{chapters/02_03_02_mean_intersection_over_union}
      \end{subsubchapter}
    \end{subchapter}

    \begin{subchapter}{Domain Adaption}
      \input{chapters/02_04_domain_adaption}

      \begin{subsubchapter}{Multi-Task Training}
        \input{chapters/02_04_01_multi_task_training}
      \end{subsubchapter}

      \begin{subsubchapter}{Weight Sharing/Network Splitting}
        \input{chapters/02_04_02_weight_sharing_network_splitting}
      \end{subsubchapter}

      \begin{subsubchapter}{Gradient Scaling}
        \input{chapters/02_04_03_gradient_scaling}
      \end{subsubchapter}

      \begin{subsubchapter}{Adversarial Training}
        \input{chapters/02_04_04_adversarial_training}
      \end{subsubchapter}
    \end{subchapter}
  \end{topchapter}

  \begin{topchapter}{Learning Setup}
    \input{chapters/03_learning_setup}

    \begin{subchapter}{Model Architecture}
      \input{chapters/03_01_model_architecture}

      \begin{subsubchapter}{Encoder Network}
        \input{chapters/03_01_01_encoder_network}
      \end{subsubchapter}

      \begin{subsubchapter}{Pose Decoder Network}
        \input{chapters/03_01_02_pose_decoder_network}
      \end{subsubchapter}

      \begin{subsubchapter}{Common Decoder Network}
        \input{chapters/03_01_03_common_decoder_network}
      \end{subsubchapter}

      \begin{subsubchapter}{Monocular Depth Estimation Output Layers}
        \input{chapters/03_01_04_monocular_depth_estimation_output_layers}
      \end{subsubchapter}

      \begin{subsubchapter}{Semantic Segmentation Output Layers}
        \input{chapters/03_01_05_semantic_segmentation_output_layers}
      \end{subsubchapter}

      \begin{subsubchapter}{Domain Discriminator Output Layers}
        \input{chapters/03_01_06_domain_discriminator_output_layers}
      \end{subsubchapter}

      \begin{subsubchapter}{Combined Learning Setup}
        \input{chapters/03_01_07_learning_setup}
      \end{subsubchapter}
    \end{subchapter}

    \begin{subchapter}{Datasets}
      \input{chapters/03_02_datasets}

      \begin{subsubchapter}{KITTI}
        \label{sec:ds_kitti}
        \input{chapters/03_02_01_kitti}
        \newpage
      \end{subsubchapter}

      \begin{subsubchapter}{Cityscapes}
        \label{sec:ds_cityscapes}
        \input{chapters/03_02_02_cityscapes}
        \newpage
      \end{subsubchapter}

      \begin{subsubchapter}{GTA-V}
        \label{sec:ds_gta5}
        \input{chapters/03_02_03_gta_v}
        \newpage
      \end{subsubchapter}

      \begin{subsubchapter}{SYNTHIA}
        \label{sec:ds_synthia}
        \input{chapters/03_02_04_synthia}
        \newpage
      \end{subsubchapter}

      \begin{subsubchapter}{Virtual KITTI}
        \label{sec:ds_virtual_kitti}
        \input{chapters/03_02_05_virtual_kitti}
        \newpage
      \end{subsubchapter}
    \end{subchapter}

    \begin{subchapter}{Training Parameters}
      \input{chapters/03_03_training_parameters}
    \end{subchapter}

    \begin{subchapter}{Evaluation Metrics}
      \input{chapters/03_04_evaluation_metrics}
    \end{subchapter}
  \end{topchapter}

  \begin{topchapter}{Experimental Evaluation}
    \input{chapters/04_experimental_evaluation}

    \begin{subchapter}{Baselines}
      \input{chapters/04_01_baselines}
    \end{subchapter}

    \begin{subchapter}{Annotated Datasets}
      \input{chapters/04_02_annotated_datasets}

      \begin{subsubchapter}{Domain Adaption - Multi-Task}
        \input{chapters/04_02_01_domain_adaption_multi_task}
      \end{subsubchapter}

      \begin{subsubchapter}{Domain Adaption - Adversarial}
        \input{chapters/04_02_02_domain_adaption_adversarial}
      \end{subsubchapter}
    \end{subchapter}

    \begin{subchapter}{Synthetic Datasets}
      \input{chapters/04_03_synthetic_datasets}

      \begin{subsubchapter}{Domain Adaption - Multi-Task}
        \input{chapters/04_03_01_domain_adaption_multi_task}
      \end{subsubchapter}
    \end{subchapter}
  \end{topchapter}

  \begin{topchapter}{Summary}
    \input{chapters/05_summary}
  \end{topchapter}

  \begin{appendixchapter}{Supplementary Material}
    \begin{subchapter}{Evaluation Images}
      \input{chapters/06_01_evaluation_images}
    \end{subchapter}

    \begin{subchapter}{Confusion Matrices}
      \input{chapters/06_02_confusion_matrices}
    \end{subchapter}

    \hashifyfile{hash.tex}
  \end{appendixchapter}

  \begin{bibliographychapter}
    \printbibliography[heading=bibintoc,title={Bibliography}]
  \end{bibliographychapter}
\end{document}
